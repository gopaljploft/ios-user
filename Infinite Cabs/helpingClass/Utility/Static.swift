//
//  Static.swift
//  Infinite Cabs
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 micro. All rights reserved.
//

import Foundation
import UIKit


class staticClass {
    
    static let shared = staticClass()
    
    let themeColor = UIColor(named: "ThemeColor")?.cgColor ?? UIColor.clear.cgColor
    
    
    class func getStoryboard_Main() -> UIStoryboard? {
        let str = "Main"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Map() -> UIStoryboard? {
        let str = "Map"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Settings() -> UIStoryboard? {
        let str = "Settings"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_MyRide() -> UIStoryboard? {
        let str = "MyRide"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
}


