//
//  Constant.swift
//  SocialHub
//
//  Created by Mac on 12/2/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

struct Constants
{
    
    
    struct AppLinks
    {
//        static let APP_MODE = "user_managment_mode"
        static let APP_MODE = "navigation_mode"
        static let APP_NAME = "Hall"
       
        //Test URL
//        static let API_BASE_URL = "http://stageofproject.com/texiapp-new/web_service/"
        
        //Live URL
        static let API_BASE_URL = "https://admin.infinitecabs.com.au/web_service/"
        static let API_BASE_URL_IMG = "https://admin.infinitecabs.com.au/"
        
        //http://stageofproject.com/texiapp-new/Apidoc
        
        
        
        //TERMS CONDITION
//        static let TermsConditionUrl = "http://stageofproject.com/infinitecabsnew/terms-and-conditions.php"
        static let TermsConditionUrl = "https://infinitecabs.com.au/legal"
        
        //ABOUT
//        static let AboutUrl = "http://stageofproject.com/infinitecabsnew/about.php"
        static let AboutUrl = "https://infinitecabs.com.au/about"
        
        //PRIVACY
//        static let PrivacyUrl = "http://stageofproject.com/infinitecabsnew/privacy.php"
        static let PrivacyUrl = "https://infinitecabs.com.au/legal"
        
        static let stripeTC = "https://stripe.com/au/legal"
        
        static let APIKEY_Google = "AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        
        static var session_Delete_Url = "" // This value will come from CheckRequestuser api response
        
    }
    
    

    enum AppMode : String
    {
        case Navigation = "navigation_mode"
        case UserManagment = "user_managment_mode"
    }
    
   
    
    
    enum ApiUrl : String {
        //user api
        case EmailExistOrNot = "emailExists"
        case SignUp = "sign_up"
        case Login = "login"
//        case Forgot = "forgot_password"
        case changePassword = "change_password"
        case notificationUpdate = "notification_update"
        
        case serchUserTexi = "serchUserTexi"
        case boookingCab = "boookingCab"
        case searchDriver = "searchDriver"
        
        
        case declinedBookings = "declinedBookings"
        case checkRequestuser = "CheckRequestuser"
        
        case resendOtp = "ResendOtp"
        case forgot_password = "forgot_password"
        case verifyOtp = "verifyOtp"
        case veriftOtpforpassword = "veriftOtpforpassword"
        case GetUserProfile = "getUserProfile"
        //veriftOtpforpassword
//        case userCompleteRide = "userUpcommingRide"
        
      //  case userCancelRide = "userCancelRide"
        
        //driverFeedback
        case driverFeedback = "driverFeedback"
        
        case get_user_driver_latlong = "get_user_driver_latlong"
          //"get_user_driver_latlong"
       // get_user_driver_latlong
        
        case promocodeList = "promocodeList"
        case applypromocode = "applypromocode"
        
        case cancelreasonList = "CancelreasonList"
        
        case getDriverMobileNumber = "CheckRequestuserSide"
        
        
//        case sessionDel  = "http://sample.jploft.com/twillo1/sess_del.php"
        
        
        case serchUserTexiAmount = "serchUserTexiAmount"
        //case getUseAddress = "getUseAddress"
        case insertUserAddress = "insertUserAddress"
        case contactus = "contactus"
        case getcardlist  = "getcardlist"
        case insertCard = "insertCard"
        
        case deleteCard = "deleteCard"
        
        
        case OtpVerify = "otpverify"
        case ResendOtp = "resendotp"
        case ProfileEdit = "profile_edit" //profile_edit
        
       
        
        
        
//        case Country = "country"
//        case State = "state"
//        case Cities = "cities"
        
        case ResendOtpforPhone = "ResendOtpforPhone"
        case VerifyOtpForPhone = "verifyOtpForPhone"
        case AddUserLocation = "addUserLocation"
        case locationList = "locationList"
        case deleteLocation = "deleteLocation"
        
        
        
        case userUpcommingRideLIST = "userUpcommingRide"
        case userCompleteRideLIST = "userCompleteRide"
        case userCancalTripLIST = "userCancelRide"
                              
        
        
    }
    
    enum UserDefult : String {
        case SaveUserData = "save-user-data-in-defult"
    }
    
    enum errorMessage: String {
        
        case NameEmpty = "Please enter name."
        case DateEmpty = "please enter expiry date."
        case CVVEmpty = "Please enter CVV."
        case validCvv = "Please enter valid CVV."
        case CreditCardNumberEmpty = "Please enter credit Card Number."
        case CreditCardNumberEnvalide = "Please enter valid credit card number."
        
        
        case FirstNameEmpty = "Please enter first name."
        case LastNameEmpty = "Please enter last name."
        case NameLimit = "Name is can not be less then 2 characters."
        case FirstNameLimit = "First Name is can not be less then 2 characters and maximum is 15 characters. "
       
        case LastNameLimit = "Last Name is can not be less then 2 characters and maximum is 15 characters."
        case DobEmpty = "Please enter BOB."
        case GenderEmpty = "Please select gender."
     //   case CountryCodeEmpty = "Country Code."
        case MobileNoEmpty = "Please enter Mobile Number."
        case MobileNoInvalid = "Mobile Number is not valid and minimum is 9 characters and maximum is 10 characters."
        case EmailInvalid = "Email is not valid"
        case EmailEmpty = "Please enter email."
        case EmailPhoneEmpty = "Please enter email or mobile."
        case MessageEmpty = "Please enter message."
        case PasswordEmpty = "Please enter password."
        case NewPasswordEmpty = "Please enter new password."
        case ConfirmPasswordEmpty = "Please enter confirm password.."//Confirm
        case PlaceNameEmpty = "Please enter Place name."
        case PlaceAddressEmpty = "Please enter Place address."
        
        case PasswordMinimum = "Password is can not be less then 8 characters and maximum is 16 characters."
        
        case PasswordDonNotMatch = "Password  don't match."
      //  case ZipCodeEmpty = "Zipcode is empty."
     ///   case ZipCodeMinimum = "Zipcode is can not be less then 4 characters and maximum is 12 characters."
    //    case OldPasswordInvalid = "Old password is empty."
        
        case OtpInvalid = "Please enter OTP."
        case TermsAndConditions = "Please select Terms and Conditions."
        
        case CountryNotSelected = "First select Country."
        case StateNotSelected = "First select State."
        case CityNotSelected = "First select City."
        
       
        
        case BankNameEmpty = "Bank name is empty."
        case AccountHolderNameEmpty = "Account holder name is empty."
        case AccountNumberEmpty = "Please enter account number"
        case BranchNameEmpty = "Branch name is empty."
        case Address  = "Please enter address"
        
        case selectStar = "Please fill rating stars"
        case leaveComment = "Please enter comment"
    }
    
    
    
    
}
