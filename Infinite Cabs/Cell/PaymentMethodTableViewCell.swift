//
//  PaymentMethodTableViewCell.swift
//  Infinite Cabs
//
//  Created by micro on 18/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
    
    // Outlets Method Cell
    
    @IBOutlet weak var imgPayMethod: UIImageView!
    @IBOutlet weak var lblMethod: UILabel!
    @IBOutlet weak var lblExpiresDate: UILabel!
    @IBOutlet weak var btnMark: UIButton!
    @IBOutlet weak var viewCard: CardView!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var btnAddPaymentMethod: UIButton!
    @IBOutlet weak var lblCardType: UILabel!
    @IBOutlet weak var stackCardNumber: UIStackView!
    @IBOutlet weak var btnChange: UIButton!
    
    
    
    
    // Outlets Head
    
    @IBOutlet weak var lblHeadDescr: UILabel!
    @IBOutlet weak var btnPhoneSelection: UIButton!
    
    @IBOutlet weak var lblChooseDiffPayMethod_App: UILabel!
    
    
    // Outlets Add Cell
    
   // @IBOutlet weak var btnAddPayMethod: UIButton!
    
    
    
    //Outlet Headll
    
    @IBOutlet weak var btnDriverDirectlty: UIButton!
    @IBOutlet weak var lblChooseDiffPayMethod_Direct: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
