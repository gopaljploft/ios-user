//
//  SelectDestinationHeadTVCell.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class SelectDestinationHeadTVCell: UITableViewCell {

    @IBOutlet weak var viewForBorder: UIView!
    @IBOutlet weak var btnHead: UIButton!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewForBorder.layer.borderColor = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1).cgColor
        viewForBorder.layer.borderWidth = 1
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
