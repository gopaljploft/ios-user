//
//  AppDelegate.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseAuth
import FirebaseCore
import UserNotifications
import Messages


@UIApplicationMain   
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {
    
    var window: UIWindow?
    static let shared = AppDelegate()
    static var appCurrentState = 1
    var locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        //   Messaging.messaging().delegate = self
        
        // config firebase
        FirebaseApp.configure()
            
        
        AppDelegate.appCurrentState = 1
        
        GMSServices.provideAPIKey("AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o")
        GMSPlacesClient.provideAPIKey("AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o")
        
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        
        // show launch Screen
        let storyboard = UIStoryboard(name: "LaunchScreenAnimation", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "LaunchScreenAnimationViewController")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        
        
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        let pushManager = PushNotificationManager(userId: deviceId)
        pushManager.registerForPushNotifications()
        
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        
        
        
        return true
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        })
    }
    
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        AppDelegate.appCurrentState = 1
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        AppDelegate.appCurrentState = 2
        
    }
    
    
    
    
    
    //    func showMapStoryBoard(){
    //        // show launch Screen
    //        let storyboard = UIStoryboard(name: "Map", bundle: nil)
    //        let initialViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController")
    //        self.window?.rootViewController = initialViewController
    //        self.window?.makeKeyAndVisible()
    //
    //    }
    
    
    
    // MARK: UISceneSession Lifecycle
    /*
     func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
     // Called when a new scene session is being created.
     // Use this method to select a configuration to create the new scene with.
     return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
     }
     
     func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
     // Called when the user discards a scene session.
     // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
     // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
     }
     */
    
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert ,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        print(userInfo)
        //userInfo["message"]
        
        
        
        completionHandler()
    }
    
}
