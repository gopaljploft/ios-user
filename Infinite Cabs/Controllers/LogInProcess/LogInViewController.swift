//
//  LogInViewController.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LogInViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnLogIn: UIButton!
    
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var passwordEyeButton: UIButton!
    
    var userEmail = ""
    
    var isPasswordIsVisible =  false
                         
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
        txtEmail.text! = userEmail
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.isNavigationBarHidden = true
        view.endEditing(true)
    }
    
    // initial SetUp
    func initialSetUp(){
        self.hideKeyboardWhenTappedAround()
        btnLogIn.layer.cornerRadius = 4
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    // MARK:- Actions
    
    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSignUpClicked(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func btnForgotPassClicked(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    @IBAction func btnLogInClicked(_ sender: UIButton)
    {
     
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if(txtEmail.text?.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.EmailPhoneEmpty.rawValue, vc: self)
        }
            //FIXME:
//        else if(HelpingMethod.shared.VaildateTxtEmail(strEmail: txtEmail.text!) ==  false)
//        {
//            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.EmailInvalid.rawValue, vc: self)
//        }
        else if(txtPassword.text?.isEmpty ==  true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.PasswordEmpty.rawValue, vc: self)
        }
        else
        {
            
             self.methodUserSignInProcess()
            
        }
    }

    
    
    @IBAction func passwordEyeButtonTapped(_ sender: Any) {
        
        
        if(isPasswordIsVisible){
            // make password inVisible and change image
            txtPassword.isSecureTextEntry = true
            passwordEyeButton.setImage(UIImage(named: "icon_eye_slash_password"), for: .normal)
            
//            if #available(iOS 13.0, *) {
//                let image = UIImage(systemName: "eye.slash.fill")
//                passwordEyeButton.setImage(image, for: .normal)
//
//            }
        }else{
            // make password visible and change image
            txtPassword.isSecureTextEntry = false
            passwordEyeButton.setImage(UIImage(named: "icon_eye_password"), for: .normal)
        
        }
        
        
        isPasswordIsVisible = !isPasswordIsVisible
        
    }
    
    
    
    //MARK:- User signIn api
    func methodUserSignInProcess()
    {
//        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let strEmail = self.txtEmail.text
        let strPassword = self.txtPassword.text
        let deviceToken = global.deviceToken
        
        let  dictKeys = ["password": strPassword,"email": strEmail , "deviceToken" : deviceToken ] as! [String:String]
        print(dictKeys as Any)
        DispatchQueue.global(qos: .background).async
            {
                
                ApiLibrary.shared.APICallingWithParameters(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.Login.rawValue, in: self) { (response,status,message) in
//                    Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                    
                    guard  let res = response as? NSDictionary else
                    {
                        return
                    }
                    
                    let statusCode = "\(String(describing: res["statusCode"]!))"
                    let strMessage = res["message"] as! String
                    
                    
                    if statusCode == "200"
                    {
                        
                        if let data = res["data"] as? NSDictionary {
                            
                            global.userId = data["id"] as! String
                            global.userMobileNumber = data["mobile"] as! String
                            global.userCountryCode = data["phoneCode"] as! String
                            global.userEmail = data["email"] as! String
                            global.userFirstName = data["first_name"] as! String
                            global.userLastName = data["last_name"] as! String
                            global.userNotification = data["notification"] as! String
//                            let imgUrl = "http://stageofproject.com/texiapp-new/user_image/"
                            let imgUrl = Constants.AppLinks.API_BASE_URL_IMG + "user_image/"
                            let imgUrl2 = data["image"] as! String
//
                            let payMode = data["paymentMode"] as! String
                            
                            
                            if payMode == "cash"{

                                UserDefaults.standard.set("Pay the Driver Directly", forKey: "PAYMENT")
                                UserDefaults.standard.set("", forKey: "CARDID")
                                
                            }else if payMode == "card"{

                                var strId = ""
                                
                                if data["cardId"] is Int{
                                    strId = String(data["cardId"] as! Int)
                                }else{
                                    strId = data["cardId"] as! String
                                }
                                
                                UserDefaults.standard.set("Pay with the App", forKey: "PAYMENT")
                                UserDefaults.standard.set(strId, forKey: "CARDID")
                                
                                if strId == "0"{
                                    UserDefaults.standard.set("Payment", forKey: "PAYMENT")
                                    UserDefaults.standard.set("", forKey: "CARDID")
                                }
                                
                            }else{
                                UserDefaults.standard.set("Payment", forKey: "PAYMENT")
                                UserDefaults.standard.set("", forKey: "CARDID")
                            }
                            
                            global.userProfileImage = imgUrl + imgUrl2
                            
                            self.showToast(message: "Login Successfully", seconds: 3)
                            
                            if(data["otpstatus"] as! String == "1"){
                                // otp varified
                                global.isUserLogin = true
                                self.shoeMapViewController()
                            }else{
                                
                                self.showVerifyPhoneVC()
                            }
                            
                            
                        }
                    
                    }
                    else
                    {
//                        Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                        HelpingMethod.shared.presentAlertWithTitle(title: "Error!", message: strMessage, vc: self)
                    }
                    
                }
        }
    }
    
    func shoeMapViewController(){
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            let strbdMap = staticClass.getStoryboard_Map()
            let vc = strbdMap?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let nav = UINavigationController(rootViewController: vc)
            nav.isNavigationBarHidden = true
            appDelegate.window!.rootViewController = nav
            appDelegate.window?.makeKeyAndVisible()
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func showVerifyPhoneVC(){
           
           DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
               let strbdMap = staticClass.getStoryboard_Main()
               let vc = strbdMap?.instantiateViewController(withIdentifier: "VerifyPhoneVC") as! VerifyPhoneVC
                vc.isWorkingVC = "login"
               self.navigationController?.pushViewController(vc, animated: true)
           }
           
       }
    
}



