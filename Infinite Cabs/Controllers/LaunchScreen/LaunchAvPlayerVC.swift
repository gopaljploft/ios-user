//
//  LaunchAvPlayerVC.swift
//  Infinite Cabs
//
//  Created by Apple on 29/05/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import AVKit

var player = AVPlayer()
class LaunchAvPlayerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
          loadVideo()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
           // Code you want to be delayed
            self.showVC()
        }
        
        
        // Do any additional setup after loading the view.
    }
    

    
    private func loadVideo() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        } catch { }

        let path = Bundle.main.path(forResource: "Taxi_Splash_Original", ofType:"mp4")

        player = AVPlayer(url: NSURL(fileURLWithPath: path!) as URL)
        let playerLayer = AVPlayerLayer(player: player)

        playerLayer.frame = self.view.frame
//        playerLayer.videoGravity = AVLayerVideoGravity.resize
        playerLayer.zPosition = -1

        self.view.layer.addSublayer(playerLayer)

        player.seek(to: CMTime.zero)
        player.play()
    }
    
    
    
    
    
    func showVC(){
        
        
        if(global.isUserLogin){
            
            let stbdHome = staticClass.getStoryboard_Map()
            
            let nextViewController = stbdHome?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            let navigationController = UINavigationController(rootViewController: nextViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.rootViewController = navigationController
            
            
//
        }
        else{
            let stbdHome = staticClass.getStoryboard_Main()
            
            let nextViewController = stbdHome?.instantiateViewController(withIdentifier: "FirstLogInViewController") as! FirstLogInViewController
                let navigationController = UINavigationController(rootViewController: nextViewController)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
                appdelegate.window?.rootViewController = navigationController
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
