//
//  ContactVC.swift
//  Infinite Cabs
//
//  Created by Apple on 04/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKCountryPicker


class ContactVC: UIViewController  {

    
    @IBOutlet weak var txtViewMsg: UITextView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var yourMessageView: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!

    var countryName = "Australia"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        yourMessageView.layer.cornerRadius = 8
        yourMessageView.layer.borderColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        yourMessageView.layer.borderWidth = 1
        
        txtViewMsg.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        
        btnSubmit.layer.cornerRadius = 5
        
        txtViewMsg.delegate = self
        txtEmail.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        guard let country = CountryManager.shared.currentCountry else {
            
            lblCountryCode.text = "+61"
            imgFlag.image = UIImage(named: "australiyaflag")
            
            return
        }
        
        lblCountryCode.text = country.dialingCode
        imgFlag.image = country.flag
        
    }
    

    //MARK:- ACTION
    
    @IBAction func btnSelectCountryClicked(_ sender: UIButton) {
            
           let _ = CountryPickerController.presentController(on: self) { (country) in
                self.imgFlag.image = country.flag
                self.lblCountryCode.text = country.dialingCode
                self.countryName = country.countryName
            }
            
    //        countryController.flagStyle = .corner
    //        countryController.isCountryDialHidden = true
            
            
        }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        
        let name = txtName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let phone = txtPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let email = txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        let placeHolderTxt = txtViewMsg.text == "Enter Message" ? "" : txtViewMsg.text
        
        if(validateMessage(txtViewMsg.text ?? "") && validateMessage(placeHolderTxt ?? "") && validateName(name ?? "") && validateEmail(email ?? "") && validateMobileNumber(phone ?? "")){
            callContactUsApi()
        }
        
    }
    
    private func callContactUsApi(){
               
//        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
         let userID = global.userId
           
        let message = txtViewMsg.text ?? ""
        let email = txtEmail.text ?? ""
        let name = txtName.text ?? ""
        let phone = txtPhoneNumber.text ?? ""
        
        
           
           

        let dictKeys = [ "user_id"  : userID ,
                         "message"  : message,
                         "email"    : email,
                         "name"     : name,
                         "phone"    : phone,
                         "phoneCode":self.lblCountryCode.text!,
                         "country":countryName
                         
           ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.contactus.rawValue
                                               
           Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
                  
//               Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
               do {
                   let json: JSON = try JSON(data: response.data!)
                                  
                   if (json["statusCode"].stringValue == "200"){
                       
                    self.showToast(message: json["message"].stringValue, seconds: 2.5)
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.2) {
                        self.navigationController?.popViewController(animated: true)
                    }
                   
                   }
                   else{
                       // error
                       self.showAlert(title: "Error", message: json["message"].stringValue)
                       }
                                         
               } catch {
                   // show a alert
//                   Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                   self.showAlert(title: "Error", message: "Please check network connection")
               }
           })
       }
       
    
}



extension  ContactVC : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if( txtViewMsg.text == "Enter Message"){
            txtViewMsg.text  = ""
        }
        txtViewMsg.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if (txtViewMsg.text == ""){
              txtViewMsg.text  = "Enter Message"
              txtViewMsg.textColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        }
    }
    
}


extension  ContactVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}
