//
//  ChangePasswordViewController.swift
//  Infinite Cabs
//
//  Created by Mac on 2/14/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: UIViewController , UITextFieldDelegate{

    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        submitButton.layer.cornerRadius = 5
        newPasswordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        if(validateNewPassword(newPasswordTextField.text ?? "") && validateConfirmPassword(confirmPasswordTextField.text ?? "")){
            
            if(newPasswordTextField.text == confirmPasswordTextField.text){
                updateUserPasswordAPI()
            }
            else{
                showToast(message: Constants.errorMessage.PasswordDonNotMatch.rawValue, seconds: 3)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    
    
    //change password
    func updateUserPasswordAPI()
            {
//                Hud.shared.showHud()
                ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
                
                let password = newPasswordTextField.text
                let userID = global.userId
                
                
                let dictKeys = [ "password" : password,  "uid" : userID ] as! [String:String]
                 print(dictKeys as Any)
        
            
                DispatchQueue.global(qos: .background).async
                    {
                        
                       ApiLibrary.shared.APICallingWithParameters(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.changePassword.rawValue, in: self) { (response,status,message) in
                        
//                        Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                            
                        
                       guard  let res = response as? NSDictionary else
                        {
                            return
                        }
                        
                        let statusCode = "\(String(describing: res["statusCode"]!))"
                        let strMessage = res["message"] as! String
                        
                        
                        if statusCode == "200"
                        {
                            
                            
                            
//                            self.dismiss(animated: true, completion: nil)
//                            self.showToast(message: "Password Change Successfully.", seconds: 3)
                            let strbd = staticClass.getStoryboard_Settings()
                            let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                            vc.delegate = self
                            vc.strTitle = "Thank You"
                            vc.strDescription = "Your password has successfully changed."
                            vc.isBottomViewHidden = false
                            self.present(vc, animated: true, completion: nil)
                        
                           
                        }
                        else
                        {
//                            HelpingMethod.shared.presentAlertWithTitle(title: "Error!", message: strMessage, vc: self)
                            self.showToast(message: strMessage, seconds: 3)
                        }
                    }
                }
            }
}


extension ChangePasswordViewController:customeAlertDelegate{
    
    func btnOkClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
