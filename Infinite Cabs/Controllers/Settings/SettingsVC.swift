//
//  SettingsVC.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit

class SettingsVC: BaseViewController {
    
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var mytable: UITableView!
    
    
    var arrOptions = ["Edit Profile","Notification","App Version","Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  addSlideMenuButton()
        // Do any additional setup after loading the view.
        
    }
    

    //MARK:- ACTIONS
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func notificationSwitchAction(_ sender: Any) {
        
        if((sender as AnyObject).isOn == true){
            print("on")
            updateNotificationAPI("2")
        }else{
            print("off")
            updateNotificationAPI("1")
        }
    }
    
    
    
    func updateNotificationAPI( _ notification : String)
     //   1 = off
        {
//                Hud.shared.showHud()
            ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
                
                let userID = global.userId
                
                
                let dictKeys = [ "notification" : notification,  "userId" : userID ] as [String:String]
                
            
                DispatchQueue.global(qos: .background).async
                    {
                        
                        ApiLibrary.shared.APICallingWithParameters(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.notificationUpdate.rawValue, in: self) { (response,status,message) in
                        
//                        Hud.shared.hideHud()
                            ActivityIndicatorWithLabel.shared.hideProgressView()
                            
                        
                        guard  let res = response as? NSDictionary else
                        {
                            return
                        }
                        
                        let statusCode = "\(String(describing: res["statusCode"]!))"
                        let strMessage = res["message"] as! String
                        
                        
                        if statusCode == "200"
                        {
                        if(notification == "1"){
                            global.isNotificationIsOff = true
                        }
                        else{
                            global.isNotificationIsOff = false
                        }
                        
                            self.dismiss(animated: true, completion: nil)
                            self.showToast(message: "Notification Update Successfully", seconds: 3)
                            
                        
                        }
                        else
                        {
                            HelpingMethod.shared.presentAlertWithTitle(title: "Error!", message: strMessage, vc: self)
                        }
                    }
                }
            }
    
    
}


// MARK:- UITablView Delegate and DataSource

extension SettingsVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Head") as! SettingsTableViewCell
        return cell.contentView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SettingsTableViewCell
        
        cell.lblOption.text = arrOptions[indexPath.row] as String
        
        if indexPath.row == 1{
            cell.btnSwitch.transform = CGAffineTransform(scaleX: 0.70, y: 0.70)
            cell.btnSwitch.isHidden = false
            
            if(global.isNotificationIsOff){
                cell.btnSwitch.isOn = false
            }
            
            
        }else{
            cell.btnSwitch.isHidden = true
        }
        if indexPath.row == 2{
            cell.appVersionLabel.isHidden = false
        }else{
            cell.appVersionLabel.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("cell clicked")
        
        
        if indexPath.row == 0{
            
            //show user profile vc
            let strbd = UIStoryboard(name: "Settings", bundle: nil)
            let vc = strbd.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
//        else if indexPath.row == {
//            
//            let strbd = UIStoryboard(name: "Settings", bundle: nil)
//            let vc = strbd.instantiateViewController(withIdentifier: "LegalTVController") as! LegalTVController
//            //vc.isComeFromSetting = true
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//        }
        
        else if indexPath.row == 3
        {
            DispatchQueue.main.async(execute: {() -> Void in
                let alertController1 = UIAlertController(title: "Confirm", message: "Do you want to logout?", preferredStyle: .alert)
                alertController1.addAction(UIAlertAction(title: "Yes", style: .default, handler: {(_ action1: UIAlertAction?) -> Void in
                    
                    global.shared.removeDataFromUserDefult()
                    
                    // Need facebook logout
                    
                    let strbdMain = staticClass.getStoryboard_Main()
                    let initialVC = strbdMain?.instantiateViewController(withIdentifier: "FirstLogInViewController") as! FirstLogInViewController
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let nav = UINavigationController(rootViewController: initialVC)
                    appDelegate.window!.rootViewController = nav
                    
                    
                    
                }))
                alertController1.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
                self.present(alertController1, animated: true) {() -> Void in }
            })
        }
        
        
        
    }
   
}
