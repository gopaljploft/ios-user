//
//  LegalVC.swift
//  Infinite Cabs
//
//  Created by Apple on 04/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import WebKit

class LegalVC: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var myWebKit: WKWebView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var isComeFromSetting = false
    var isPrivacy = false
    var isTermsCond = false
    var isAboutUs = false
    var isStripeTC = false
    
    var aboutUs = Constants.AppLinks.AboutUrl
    
    var legalVcUrl = "https://infinitecabs.com.au/"
    
    var privacyPolicy = Constants.AppLinks.PrivacyUrl
    
    var termsAndCond = Constants.AppLinks.TermsConditionUrl
    
//    var stripeTermsAndCond = Constants.AppLinks.stripeTC
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myWebKit.navigationDelegate = self
        
        var urlToShowWebPage = ""
        
        if isAboutUs{
            titleLabel.text = "About Us"
            urlToShowWebPage = aboutUs
            
        }else if isPrivacy{
            
            titleLabel.text = "Privacy Policy"
            urlToShowWebPage = privacyPolicy
            
        }else if isTermsCond{
            
            titleLabel.text = "Terms & Conditions"
            urlToShowWebPage = termsAndCond
            
        }else if isStripeTC{
         
            titleLabel.text = "Terms & Conditions"
            urlToShowWebPage = termsAndCond
            
        }else{
            
            titleLabel.text = "Legal"
            urlToShowWebPage = legalVcUrl
            
        }
        
        let _url = URL(string: urlToShowWebPage)!
        myWebKit.load(URLRequest(url: _url))
        myWebKit.allowsBackForwardNavigationGestures = true
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    //MARK:- ACTION
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
