//
//  LaunchScreenAnimationViewController.swift
//  Infinite Cabs
//
//  Created by Mac on 2/11/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class LaunchScreenAnimationViewController: UIViewController {

    @IBOutlet weak var animationView: UIView!
    
    
    @IBOutlet weak var carImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        carImage.layer.zPosition = 1
        
        let imageView = UIView()
        imageView.frame = CGRect(x: 0, y: 85, width: 1500, height: 5)
        
        let roadImage1 = UIImageView()
        roadImage1.contentMode = .scaleToFill
        roadImage1.image = UIImage(named: "stripe_new")
//        stripe_a  old one
        
        let roadImage2 = UIImageView()
        roadImage2.contentMode = .scaleToFill
        roadImage2.image = UIImage(named: "stripe_new")
        
        let roadImage3 = UIImageView()
        roadImage3.contentMode = .scaleToFill
        roadImage3.image = UIImage(named: "stripe_new")
        
        //scrolling_foreground
        
        animationView.addSubview(imageView)
        
        imageView.addSubview(roadImage1)
        imageView.addSubview(roadImage2)
        imageView.addSubview(roadImage3)
        
        roadImage1.frame = CGRect(x: 0, y: 0, width: 500, height: 5)
        roadImage2.frame = CGRect(x: 500, y: 0, width: 500, height: 5)
        roadImage2.frame = CGRect(x: 1000, y: 0, width: 500, height: 5)
        
        
        let bgImage = UIImageView()
        bgImage.contentMode = .scaleToFill
        bgImage.image = UIImage(named: "scrolling_foreground")
        
        bgImage.frame = CGRect(x: 0, y:50, width: 1500, height: 25)
        animationView.addSubview(bgImage)
        
        UIView.animate(withDuration: 3.5) {
            imageView.center = CGPoint(x: -500, y: 87.5)
            bgImage.center = CGPoint(x: -100, y: 62.5)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
           // Code you want to be delayed
            self.showVC()
        }
    }
    
    
    
    
    func showVC(){
        
        
        if(global.isUserLogin){
            let stbdHome = staticClass.getStoryboard_Map()
            
            let nextViewController = stbdHome?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            let navigationController = UINavigationController(rootViewController: nextViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.rootViewController = navigationController
        }
        else{
            let stbdHome = staticClass.getStoryboard_Main()
            
            let nextViewController = stbdHome?.instantiateViewController(withIdentifier: "FirstLogInViewController") as! FirstLogInViewController
                let navigationController = UINavigationController(rootViewController: nextViewController)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
                appdelegate.window?.rootViewController = navigationController
        }
    }
}
