//
//  MyProfileVC.swift
//  Infinite Cabs
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import SDWebImage
import SKCountryPicker
import Alamofire
import SwiftyJSON

class MyProfileVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var imgProfle: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var headingUserName: UILabel!
    @IBOutlet weak var emailChackImage: UIImageView!
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    var imagePicker:ImagePicker!
    
    var countryName = "Australia"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setup_UI()
        
        emailChackImage.layer.cornerRadius = emailChackImage.frame.height/2
        
  
        txtName.text = global.userFirstName
        txtLastName.text = global.userLastName
        txtEmail.text = global.userEmail
        txtPhoneNumber.text = global.userMobileNumber

        headingUserName.text = "Hey " + global.userFirstName + "!"
        
        
        let img =  global.userProfileImage
        
        
        if let imageURL = URL(string: img) {
            imgProfle.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "user_Ic"), options: SDWebImageOptions.progressiveLoad) { (image, error, sdimagecacheType, url) in
                
            }
        }
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        getProfile()
    }
    
    
    // initial UI setup
    func setup_UI(){
        
        btnSave.layer.cornerRadius = 4
        btnChangePassword.layer.cornerRadius = 4
        imgProfle.layer.cornerRadius = imgProfle.frame.height/2
        
        txtName.delegate = self
        txtEmail.delegate = self
        txtPhoneNumber.delegate = self
        txtLastName.delegate = self
        
        
//        guard let country = CountryManager.shared.currentCountry else {
//
//            lblCountryCode.text = "+61"
//            imgFlag.image = UIImage(named: "australiyaflag")
//
//            return
//        }
//
//        lblCountryCode.text = country.dialingCode
//        imgFlag.image = country.flag
        
        
        //txtName.setBottomBorder()
        //   txtLastName.setBottomBorder()
        //   txtEmail.setBottomBorder()
        //   txtPhoneNumber.setBottomBorder()
        
    }
    
    
    
    private func getProfile(){
        
        //  http://stageofproject.com/texiapp-new/web_service/ResendOtp
        //userId
        //        Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let userID = global.userId
        let dictKeys = [ "user_id" : userID ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.GetUserProfile.rawValue
        print("param", dictKeys, "url",url)
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                print(json)
                if (json["statusCode"].stringValue == "200"){
                    
                    
                    let phonecode = json["data"]["phoneCode"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
                
                    
                    guard phonecode != "" else {
                       return
                    }
                   
                    
                    if let countryDetail = CountryManager.shared.country(withDigitCode: phonecode)
                    {
                        self.imgFlag.image = countryDetail.flag
                        self.lblCountryCode.text = "+" + (countryDetail.digitCountrycode ?? "")
                        self.countryName = countryDetail.countryName
                    }
                    
                    
//                    self.showToast(message: "New code has been sent successfully on you mobile number.", seconds: 2)
                }
                else{
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                    Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    //MARK:- ACTION
    
    @IBAction func btnSelectCountryClicked(_ sender: UIButton) {
            
           let _ = CountryPickerController.presentController(on: self) { (country) in
                self.imgFlag.image = country.flag
                self.lblCountryCode.text = country.dialingCode
                self.countryName = country.countryName
            }
            
    //        countryController.flagStyle = .corner
    //        countryController.isCountryDialHidden = true
            
            
        }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func editProfileButtonTapped(_ sender: UIButton) {
        //
        self.imagePicker.present(from: sender)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        
        if(validateFirstName(txtName.text ?? "") && validateLastName(txtLastName.text ?? "")  && validateEmail(txtEmail.text ?? "") && validateMobileNumber(txtPhoneNumber.text ?? "")){
            
            updateUserDataAPI()
        }
        
    }
    
    
    @IBAction func changePasswordButtonTapped(_ sender: Any) {
        
        let strbd = UIStoryboard(name: "Settings", bundle: nil)
        let vc = strbd.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func updateUserDataAPI()
    {
        //                Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        let FirstName = self.txtName.text
        let LastName = self.txtLastName.text
        let Email = self.txtEmail.text
        let mobilenumber = self.txtPhoneNumber.text
        
        let userID = global.userId
        
        
        let dictKeys = [ "uid" : userID,  "first_name" : FirstName,"last_name": LastName,
                         "email": Email,"mobile":mobilenumber,"isdevice": "IOS","phoneCode":self.lblCountryCode.text!,"country":countryName] as! [String:String]
        
        let imgdata = imgProfle.image?.pngData()
        
        print(dictKeys as Any)
        // var data : Data!
        //            if(self.isImageUpload)
        //            {
        //                data = imgProfile.image?.jpegData(compressionQuality: 0.6)
        //            }
        
        DispatchQueue.global(qos: .background).async
            {
                
                ApiLibrary.shared.imageUploadKeys(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.ProfileEdit.rawValue, image: imgdata, imageKey: "image", in: self, showHud: true) { (response,status,message, statusCode, phone) in
                    
                    
                    // ApiLibrary.shared.APICallingWithParameters(postDictionary: dictKeys, strApiUrl: Constants.ApiUrl.ProfileEdit.rawValue, in: self) { (response,status,message) in
                    
                    
                    
                    //                        Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                    
                    
                    guard  let res = response as? NSDictionary else
                    {
                        return
                    }
                    
                    if  (res.count > 0)
                    {
                        if statusCode == "200"{
                            
                            
                            let data = res
                            
                            global.userId = data["id"] as? String ?? ""
                            global.userMobileNumber = data["mobile"] as? String ?? ""
                            global.userEmail = data["email"] as? String ?? ""
                            global.userFirstName = data["first_name"] as? String ?? ""
                            global.userLastName = data["last_name"] as? String ?? ""
                            global.userCountryCode = res["phoneCode"] as? String ?? ""
                            
                            /*
                            let phonecode = res["phoneCode"] as! String
                            
                            if let countryDetail = CountryManager.shared.country(withDigitCode: phonecode)
                            {
                                self.imgFlag.image = countryDetail.flag
                                self.lblCountryCode.text = countryDetail.digitCountrycode
                            }
                            else
                            {
                                
                            }
                            */
                            let imgUrl = data["imagepath"] as! String
                            let imgUrl2 = data["image"] as! String
                            
                            global.userProfileImage = imgUrl + imgUrl2
                            self.dismiss(animated: true, completion: nil)
                            
                            
                            //                                self.showToast(message: "Your profile details have been successfully updated.", seconds: 3)
                            let strbd = staticClass.getStoryboard_Settings()
                            let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                            vc.strTitle = "Thank You"
                            vc.strDescription = "Your profile has successfully updated."
                            vc.delegate = self
                            self.present(vc, animated: true, completion: nil)
                            
                        }else if statusCode == "201"{
                            
                            let strbd = staticClass.getStoryboard_Settings()
                            let vc = strbd?.instantiateViewController(withIdentifier: "VerifyPhoneOnChangedVC") as! VerifyPhoneOnChangedVC
                            vc.strPhone = phone
                            vc.strCountryCode = self.lblCountryCode.text!
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                        
                        
                    }
                    else
                    {
                        //                            Hud.shared.hideHud()
                        ActivityIndicatorWithLabel.shared.hideProgressView()
                        //                            HelpingMethod.shared.presentAlertWithTitle(title: "Error!", message: "", vc: self)
                        self.showToast(message:res["message"] as! String, seconds: 3)
                        
                    }
                }
        }
    }
}


extension MyProfileVC : ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.imgProfle.image = image
    }
}


extension MyProfileVC: customeAlertDelegate{
    
    func btnOkClicked() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
