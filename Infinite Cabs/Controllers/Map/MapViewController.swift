//
//  MapViewController.swift
//  Infinite Cabs
//
//  Created by micro on 18/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire
import FirebaseCrashlytics
import SVProgressHUD
import SwiftLocation



class MapViewController: BaseViewController,GMSMapViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate{
    
    
    var callBackTime : Double = 5.0
    
    static var AppData : [String : String] = ["bookingId" : "" , "driverId" : "" , "cabId:" : "" ,"driverName" : "" ,"driverRating" : "", "carNumber" : "" , "carName" : "" ,"carImage" : "" , "deiverImage" : "" ,"time" : "" , "pincode" : "" ]
    
    
    //MARK:- Outlets
    
    // date and time piker
    @IBOutlet weak var dateAndTimePikerBackgroundView: UIView!
    @IBOutlet weak var dateAndTimePikerView: UIView!
    @IBOutlet weak var dateAndTimePiker: UIDatePicker!
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewReady: UIView!
    @IBOutlet weak var viewConfirm: UIView!
    @IBOutlet weak var viewCallDriver: UIView!
    @IBOutlet weak var bottomButtonView: UIView!
    @IBOutlet weak var btnReady: UIButton!
    @IBOutlet weak var btnRideLater: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCalldriver: UIButton!
    @IBOutlet weak var btnCancelRide: UIButton!
    @IBOutlet weak var viewForTable: UIView!
    @IBOutlet weak var viewToast:UIView!
    @IBOutlet weak var lblToast:UILabel!
    @IBOutlet var imgTick: UIImageView!
    
    
    @IBOutlet weak var superViewForTabel: CardView!
    @IBOutlet weak var txtPickUpLocation: UITextField!
    @IBOutlet weak var txtDestination: UITextField!
    @IBOutlet weak var NSLayoutHeightView: NSLayoutConstraint!
    @IBOutlet weak var bottomButtonViewBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var getLocationButton: UIButton!
    
    
    
    
    //MARK:- Static VARABLE
    
    static var driverCurrentData = JSON()
    
    // location
    static var sourceLocation = CLLocationCoordinate2D()
    static var selectedSourceLocation = CLLocationCoordinate2D()
    static var selectedDestinationLocation = CLLocationCoordinate2D()
    static var pincode = ""
    
    static var sourceLocationNameTemp = ""
    static var selectedSourceLocationName = ""
    static var selectedDestinationLocationName = ""
    
    // date and time variable
    static var date : String = ""
    static var time : String = ""
    
    //marker
    static let userCurrentLocationMarker = GMSMarker()
    static let driverCurrentLocationMarker = GMSMarker()
    
    static let userPickUpLocationMarker = GMSMarker()
    static let userDropLocationMarker = GMSMarker()
    
    //    static let driverStartLocationMarker = GMSMarker()
    static let driverEndLocationMarker = GMSMarker()
    
    
    
    static var availableCarsMarkerArray = [GMSMarker]()
    
    static var polyline = GMSPolyline()
    static var polylineForDriver = GMSPolyline()
    
    
    
    static var isDriverPathCreated = false
    static var isUserPathCreated = false
    static var isWorkingOnUserRide = true
    
    
    static var userCanCreateNewRide = true
    static var isSearchDriverAgain = true
    
    static var userPathCoordinatesArray = [CLLocationCoordinate2D]()
    static var driverPathCoordinatesArray = [CLLocationCoordinate2D]()
    static var testPathCoordinatesArray = [CLLocationCoordinate2D]()
    
    
    
    //MARK:- VARIABLE
    
    var locationManager = CLLocationManager()
    
    var carsAvailableNearUser = JSON()
    
    static var selectedCarIndex = -1
    
    static var isSelectCarTableViewIsOpen = false
    
    static var noteForDriver = ""
    
    static var selectedRideMaxAmount = ""
    static var selectedRideMinAmount = ""
    static var selectedRideAmountAfterPromo = ""
    static var coupanCode = ""
    
    static var driverMobileNumber = ""
    
    
    static var mapVcViewState = 0
    
    
    var isWorkingOnTime = false
    var isRideLaterSelected = false
    
    
    
    
    // payment type
    static var paymentType  = "Payment"
    static var paymentCardId  = ""
    static var isGiocondaApiHit = false
    static var isPromoApplied = false
    
    var timer:Timer?
    var timeCount = 0
    
    var isCanceled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
        
        setUpGoogleMap()
        initialSetUp()
        setBorderAndCornerRadius()
        
        //        CheckRequestuserAPI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MapViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MapViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        dateAndTimePiker.minimumDate = Date()
        dateAndTimePiker.locale = Locale(identifier: "en_GB")
        
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        bottomButtonViewBottomHeight.constant = 245
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        bottomButtonViewBottomHeight.constant = 10
    }
    
    func setUpGoogleMap(){
        
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
//        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startMonitoringSignificantLocationChanges()
        self.mapView.isMyLocationEnabled = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        
        if (self.txtPickUpLocation.text == "" && AppDelegate.appCurrentState  ==  1){
            API_ReverseGeoCoding { (success) in
                self.CheckRequestuserAPI()
            }
        }
        
        
        
        
        // set user pickup location marker // sorce location
        createMarkerOnMap(imageName: "source_marker_1" , markerName: MapViewController.userPickUpLocationMarker, isHide: true, width: 36, height: 54 )
        
        // set user drop location marker // destination location
        createMarkerOnMap(imageName: "destination_marker_1" , markerName: MapViewController.userDropLocationMarker, isHide: true, width: 36, height: 54 )
        
        // user current location marker
        createMarkerOnMap(imageName: "current_location_mark" , markerName: MapViewController.userCurrentLocationMarker, isHide: true,width: 40, height: 50  )
        
        // driver start  location marker
        //        createMarkerOnMap(imageName: "source_marker_1" , markerName: MapViewController.driverStartLocationMarker, isHide: true )
        
        // driver end location marker
        createMarkerOnMap(imageName: "source_marker_1" , markerName: MapViewController.driverEndLocationMarker, isHide: true )
        
        createMarkerOnMap(imageName: "yellow_taxi" , markerName: MapViewController.driverCurrentLocationMarker, isHide: true )
    }
    
    
    
    
    
    func createMarkerOnMap(imageName : String , markerName :  GMSMarker , isHide : Bool, width:Int = 50, height:Int = 50) {
        
        let markerImage = UIImage(named: imageName)!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: markerImage)
        markerView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        markerName.iconView = markerView
        markerName.map = mapView
        markerName.map?.isHidden = isHide
        mapView.selectedMarker = markerName
        
    }
    
    func changeMarkerImage(imageName : String){
        
        let markerImage = UIImage(named: imageName)!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: markerImage)
        markerView.frame = CGRect(x: 0, y: 0, width: 36, height: 54)
        MapViewController.userDropLocationMarker.iconView = markerView
        
    }
    
    
    
    func setBorderAndCornerRadius(){
        //UI
        
        btnReady.layer.cornerRadius = 8
        btnRideLater.layer.cornerRadius = 8
        btnConfirm.layer.cornerRadius = 8
        btnCancel.layer.cornerRadius = 8
        btnCalldriver.layer.cornerRadius = 8
        btnCancelRide.layer.cornerRadius = 8
        //        bottomButtonView.layer.cornerRadius = 8
        
        
        btnRideLater.layer.borderColor = #colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)
        btnRideLater.layer.borderWidth = 1
        
        btnCancel.layer.borderColor = #colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)
        btnCancel.layer.borderWidth = 1
        
        
        btnCancelRide.layer.borderColor = #colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1)
        btnCancelRide.layer.borderWidth = 1
        
        // border form views
        
        //        bottomButtonView.layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
        //        bottomButtonView.layer.borderWidth = 1
        
    }
    
    func initialSetUp(){
        
        //        viewToast.layer.cornerRadius = 28
        //        viewToast.clipsToBounds = true
        
        addSlideMenuButton(_view: navView)
        navView.layer.cornerRadius = navView.frame.height/2
        
        txtPickUpLocation.delegate = self
        txtDestination.delegate = self
        
        // Add Subview
        let controller =
            self.storyboard!.instantiateViewController(withIdentifier: "MainHomeTableVC") as!
            MainHomeTableVC
        //  controller.view.layer.cornerRadius = 8
        controller.mapView = self.view
        controller.view.frame = self.viewForTable.bounds
        controller.willMove(toParent: self)
        
        self.viewForTable.addSubview(controller.view)
        self.addChild(controller)
        controller.didMove(toParent: self)
        
        
        
        //Notification Observer
        NotificationCenter.default.addObserver(self, selector: #selector(btnHeadClicked), name: Notification.Name("Head Clicked"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showChoosedCarDetail), name: Notification.Name("Car Selected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showDriverDetail), name: Notification.Name(rawValue: "ShowDriverDetail"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showDateAndTimePiker(_:)), name: Notification.Name(rawValue: "ShowDateAndTimePiker"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applyCoupnCode(_:)), name: Notification.Name(rawValue: "applyCouponCode"), object: nil)
        
        //resetMapViewControllerAllData
        NotificationCenter.default.addObserver(self, selector: #selector(resetMapViewController), name: Notification.Name(rawValue: "resetMapViewControllerAllData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(callgetDriverCurrentDataApi), name: Notification.Name(rawValue: "paymentSuccess"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(boookingCabAPI), name: Notification.Name(rawValue: "callBookingCabApi"), object: nil)
        
        
        //        let strbd = staticClass.getStoryboard_Main()
        //        let vc = strbd?.instantiateViewController(withIdentifier: "DriverRatingVC") as! DriverRatingVC
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @objc func callgetDriverCurrentDataApi(){
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4) {
            self.CheckRequestuserAPI()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        manageViewStage()
        getCurrentDateAndTime()
        manageMapElements()
        //showRideWaitOrCancelPopUp()
    }
    
    func showRideWaitOrCancelPopUp(){
        
        let strbd = staticClass.getStoryboard_Map()
        
        let vc = strbd?.instantiateViewController(withIdentifier: "WaitAndCancelRideVC") as! WaitAndCancelRideVC
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        vc.delegate = self
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    
    func getCurrentDateAndTime(){
        
        let timeFormatter = DateFormatter()
        // timeFormatter.timeStyle = DateFormatter.Style.short
        timeFormatter.dateFormat = "HH:mm"
        let strDate = timeFormatter.string(from: Date())
        
        MapViewController.time = strDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let selectedDate = dateFormatter.string(from: Date())
        MapViewController.date = selectedDate
        
    }
    
    
    func manageViewStage(){
        
        if(MapViewController.mapVcViewState  == 0 ){
            
            if(MapViewController.selectedSourceLocation.latitude != 0 &&  MapViewController.selectedDestinationLocation.latitude != 0){
                if(MapViewController.selectedCarIndex == -1){
                    btnHeadClicked()
                    return
                }
            }
            
            showChoosedCarDetail()
        }
        
        if(MapViewController.mapVcViewState  == 1 ){
            showTimeMoneyDetail()
        }
        if(MapViewController.mapVcViewState  == 2 ){
            showDriverDetail()
        }
    }
    
    func manageMapElements(){
        
        txtDestination.text = MapViewController.selectedDestinationLocationName
        txtPickUpLocation.text = MapViewController.selectedSourceLocationName
        
        if(txtPickUpLocation.text == ""){
            txtPickUpLocation.text = MapViewController.sourceLocationNameTemp
        }
        
        
        if(MapViewController.selectedSourceLocation.latitude != 0 &&  MapViewController.selectedDestinationLocation.latitude != 0){
            
            
            if( MapViewController.isWorkingOnUserRide){
                // user ride start
                
                if (!MapViewController.isUserPathCreated){
                    
                    // show user pickup and drop location marker
                    MapViewController.userPickUpLocationMarker.map = mapView
                    MapViewController.userDropLocationMarker.map = mapView
                    
                    if MapViewController.driverCurrentData["status"].stringValue == "Start"{
                        MapViewController.userPickUpLocationMarker.map = nil
                        changeMarkerImage(imageName: "source_marker_1")
                    }else{
                        changeMarkerImage(imageName: "destination_marker_1")
                    }
                    
                    MapViewController.userPickUpLocationMarker.position = MapViewController.selectedSourceLocation
                    MapViewController.userDropLocationMarker.position = MapViewController.selectedDestinationLocation
                    
                    
                    
                    var bounds = GMSCoordinateBounds()
                    
                    bounds = bounds.includingCoordinate(MapViewController.userPickUpLocationMarker.position)
                    bounds = bounds.includingCoordinate(MapViewController.userDropLocationMarker.position)
                    
                    let update = GMSCameraUpdate.fit(bounds, withPadding: 140)
                    mapView.animate(with: update)
                    
                    
                    
                    // hide driver data
                    MapViewController.polylineForDriver.map = nil
                    MapViewController.driverEndLocationMarker.map = nil//?.isHidden = true
                    //                    MapViewController.driverStartLocationMarker.map = nil //?.isHidden = true
                    MapViewController.driverCurrentLocationMarker.map = nil
                    
                    
                    // and hide user current location marker
                    MapViewController.userCurrentLocationMarker.map = nil //?.isHidden = true
                    
                    
                    // fetch path and draw on map
                    fetchRoute(from: MapViewController.selectedSourceLocation, to: MapViewController.selectedDestinationLocation, isPathForUser: true)
                    
                }
                
                
                
            }
            else{
                // driver comming
                
            }
            
            
            //TODO
            /*
             var bounds = GMSCoordinateBounds()
             for location in locationArray
             {
             let latitude = location.valueForKey("latitude")
             let longitude = location.valueForKey("longitude")
             
             let marker = GMSMarker()
             marker.position = CLLocationCoordinate2D(latitude:latitude, longitude:longitude)
             marker.map = self.mapView
             bounds = bounds.includingCoordinate(marker.position)
             }
             let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
             mapView.animate(with: update)
             */
            // manage camera
            let camera = GMSCameraPosition.camera(withLatitude: MapViewController.selectedSourceLocation.latitude,
                                                  longitude: MapViewController.selectedSourceLocation.longitude, zoom: 0)
            
            if mapView.isHidden {
                mapView.isHidden = false
                mapView.camera = camera
            } else {
                //mapView.animate(to: camera)
            }
            // hide avalabel car around user
            
            // remove all car  marker to map
            for i in 0..<MapViewController.availableCarsMarkerArray.count{
                MapViewController.availableCarsMarkerArray[i].map = nil
            }
            
            
        }else{
            // user don't select pickup and drop location
            
            // hide path
            MapViewController.polyline.map = nil
            MapViewController.polylineForDriver.map = nil
            
            //hide user pickup and drop marker
            MapViewController.userPickUpLocationMarker.map = nil//?.isHidden = true
            MapViewController.userDropLocationMarker.map = nil //?.isHidden = true
            
            //hide driver pickup and drop marker // current location
            MapViewController.driverEndLocationMarker.map = nil//?.isHidden = true
            //            MapViewController.driverStartLocationMarker.map = nil //?.isHidden = true
            MapViewController.driverCurrentLocationMarker.map = nil
            
            
            // show user current location marker
            MapViewController.userCurrentLocationMarker.map = mapView//?.isHidden = false
            
        }
        
        
        self.view.layoutIfNeeded()
    }
    
    private func CheckRequestuserAPI(){
        //  Hud.shared.showHud()
        
        let userID = global.userId
        let dictKeys = [ "user_id" : userID
        ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.checkRequestuser.rawValue//Constants.ApiUrl.checkRequestuser.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //                Hud.shared.hideHud()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    Constants.AppLinks.session_Delete_Url = json["startCall"].stringValue
                    
                    let tempbookingId = json["bookingData"]["bookingId"].stringValue
                    let tempdriverId = json["bookingData"]["driverId"].stringValue
                    
                    
                    MapViewController.AppData.updateValue( tempbookingId, forKey: "bookingId")
                    MapViewController.AppData.updateValue( tempdriverId, forKey: "driverId")
                    
                    MapViewController.AppData.updateValue(json["bookingData"]["driverName"].stringValue, forKey: "driverName" )
                    MapViewController.AppData.updateValue(json["bookingData"]["userstarrating"].stringValue, forKey: "driverRating" )
                    
                    
                    MapViewController.AppData.updateValue(json["bookingData"]["driverCar_No"].stringValue, forKey: "carNumber" )
                    MapViewController.AppData.updateValue(json["bookingData"]["driverCar_Model"].stringValue, forKey: "carName" )
                    
                    MapViewController.AppData.updateValue(json["bookingData"]["carImage"].stringValue, forKey: "carImage" )
                    //TODO//carimage
                    //   MapViewController.AppData.updateValue(json["bookingData"]["driverId"].stringValue, forKey: "carImage" )
                    MapViewController.AppData.updateValue(json["bookingData"]["driverImage"].stringValue, forKey: "deiverImage" )
                    
                    
                    if(json["bookingData"]["status"].stringValue == "New" || json["bookingData"]["status"].stringValue == "Declined") || (json["bookingData"]["status"].stringValue == "Completed") {
                        
                        MapViewController.userCanCreateNewRide = true
                    }else{
                        MapViewController.userCanCreateNewRide = false
                    }
                    
                    
                    if(json["bookingData"]["status"].stringValue == "Completed" ){
                        
                        //TODO:
                        
                        if MapViewController.driverCurrentData["payStatus"].stringValue == "2"{
                            
                            
                            DispatchQueue.main.async {
                                
                                MapViewController.userCanCreateNewRide = false
                                let strbd = staticClass.getStoryboard_Settings()
                                let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                                vc.strTitle = "Thank you for using Infinite Cabs"
                                vc.strDescription = "Trip Completed\n App payment error occur. Please pay the Cash directly to the Driver"
                                vc.strBtnOkTitle = "Driver Rating"
                                vc.btnOkWidth = 80
                                vc.delegate = self
                                self.present(vc, animated: true, completion: nil)
                                
                            }
                            
                            
                        }else{
                            
                            
                            self.API_sessionDel{ (success) in
                                
                                if success{
                                    
                                    DispatchQueue.main.async {
                                        
                                        MapViewController.userCanCreateNewRide = false
                                        let strbd = staticClass.getStoryboard_Settings()
                                        let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                                        vc.strTitle = "Thank you for using Infinite Cabs"
                                        vc.strDescription = "Your ride has successfully completed. Please rate your driver now."
                                        vc.strBtnOkTitle = "Driver Rating"
                                        vc.btnOkWidth = 140
                                        //                                    vc.imgPop = #imageLiteral(resourceName: "check_ic")
                                        vc.delegate = self
                                        self.present(vc, animated: true, completion: nil)
                                        
                                    }
                                    
                                    
                                }else{}
                                
                            }
                            
                        }
                        
                    }else if(json["bookingData"]["status"].stringValue == "Search" ){
                        
                        self.showRideWaitOrCancelPopUp()
                        
                    } else
                    
                    
                    if( json["bookingData"]["status"].stringValue == "Accepted" || json["bookingData"]["status"].stringValue == "Start" ||
                            json["bookingData"]["status"].stringValue == "paymentPending"){
                        
                        if json["bookingData"]["status"].stringValue == "paymentPending"{
                            //TODO:
                            Hud.shared.showHudWithMsg("Please wait processing payment")
                            //Please wait processing payment
                            
                        }else{
                            
                            
                            
                            let lat = json["bookingData"]["pickuplat"].doubleValue
                            let long = json["bookingData"]["pickuplng"].doubleValue
                            
                            let sourceLocation = CLLocationCoordinate2DMake( lat, long)
                            MapViewController.selectedSourceLocation = sourceLocation
                            
                            let latD = json["bookingData"]["droplat"].doubleValue
                            let longD = json["bookingData"]["droplng"].doubleValue
                            
                            let destinationLocation = CLLocationCoordinate2DMake( latD, longD)
                            MapViewController.selectedDestinationLocation = destinationLocation
                            
                            MapViewController.selectedSourceLocationName =  json["bookingData"]["pickupLocation"].stringValue
                            
                            MapViewController.selectedDestinationLocationName =  json["bookingData"]["dropLocation"].stringValue
                            
                            self.txtPickUpLocation.text = json["bookingData"]["pickupLocation"].stringValue
                            self.txtDestination.text = json["bookingData"]["dropLocation"].stringValue
                            
                            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ShowDriverDetail")))
                            NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                            self.getDriverCurrentData()
                        }
                        
                    }else{
                        // get avelibale car around you
                        self.getAllCarDataAvailableNearUserAPI()
                    }
                    
                    
                }
                
                
                
                else if (json["statusCode"].stringValue == "201"){
                    
                }
                else {
                    // error
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                     Hud.shared.hideHud()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
        
        
        
    }
    
    //MARK:- ACTION
    
    @IBAction func btnReadyNowClicked(_ sender: UIButton) {
        
        if(MapViewController.selectedCarIndex == -1){
            //            showToast(message: "Please select a car before booking", seconds: 2)
            //            showToastNew(message: "Please choose a ride before to continue with the booking.")
            
            showCustomPopUp(lbltext: "Please choose a ride before to continue with the booking.", img: UIImage(named: "information")!, bgColor: .red)
            
            return
        }
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        let strDate = timeFormatter.string(from: dateAndTimePiker.date)
        MapViewController.time = strDate
        
        MapViewController.isSelectCarTableViewIsOpen = false
        isRideLaterSelected = false
        NotificationCenter.default.post(Notification(name: Notification.Name("ReadyClicked")))
        showTimeMoneyDetail()
    }
    
    @IBAction func btnRideLaterClicked(_ sender: UIButton) {
        
        if(MapViewController.selectedCarIndex == -1){
            //            showToast(message: "Please select a car before booking", seconds: 2)
            //            showToastNew(message: "Please choose a ride before to continue with the booking.")
            showCustomPopUp(lbltext: "Please choose a ride before to continue with the booking.", img: UIImage(named: "information")!, bgColor: .red)
            return
        }
        
        MapViewController.isSelectCarTableViewIsOpen = false
        
        isRideLaterSelected = true
        NotificationCenter.default.post(Notification(name: Notification.Name("ReadyClicked")))
        showTimeMoneyDetail()
    }
    
    
    @IBAction func btnConfirmClicked(_ sender: UIButton) {
        
        let paymentStatus = UserDefaults.standard.value(forKey: "PAYMENT") as! String
        //        if(MapViewController.paymentType == "Payment")
        if(paymentStatus == "Payment"){
            //            showToast(message: "Please Select A Payment Methode", seconds: 2)
            //            showToastNew(message: "Please choose a ride before to continue with the booking.")
            showCustomPopUp(lbltext: "Please choose a ride before to continue with the booking.", img: UIImage(named: "information")!, bgColor: .red)
            return
        }
        
        let strbdMap = staticClass.getStoryboard_Map()
        let childVC = strbdMap?.instantiateViewController(withIdentifier: "DriverNoteVC") as! DriverNoteVC
        self.addChld(childVC)
        
        childVC.view.frame = self.view.frame
        view.addSubview(childVC.view)
        childVC.didMove(toParent: self)
    }
    
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        
        // fatalError()
        
        NotificationCenter.default.post(Notification(name: Notification.Name("CancelButtonClicked")))
        showChoosedCarDetail()
        MapViewController.isPromoApplied = false
        MapViewController.coupanCode = ""
        
        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
        
        
    }
    
    
    
    @IBAction func btnCallDriverClicked(_ sender: UIButton) {
        //TODO:-call (in app call)
        //
        //        let strbd = staticClass.getStoryboard_Main()
        //        let vc = strbd?.instantiateViewController(withIdentifier: "DriverRatingVC") as! DriverRatingVC
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        //        getDriverMobileNumber()
        self.callNumber(phoneNumber: "+61280741930")
        
    }
    
    func showCustomPopUp(lbltext:String, img:UIImage, bgColor:UIColor){
        
        DispatchQueue.main.async {
            
            self.lblToast.text = lbltext
            self.imgTick.image = img//UIImage(named: "information")
            self.viewToast.backgroundColor = bgColor
            self.view.bringSubviewToFront(self.viewToast)
            
            UIView.animate(withDuration: 2, delay: 0, options: .transitionFlipFromBottom, animations: {
                
                self.viewToast.isHidden = false
                
            }) { (completed) in
                self.viewToast.layer.cornerRadius = self.viewToast.bounds.height/2
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.viewToast.isHidden = true
                }
            }
            
        }
        
    }
    
    
    private func callNumber(phoneNumber: String) {
        if let url = URL(string: "telprompt://\(phoneNumber)") {
            let application = UIApplication.shared
            guard application.canOpenURL(url) else {
                return
            }
            application.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    
    @IBAction func btnCancelRideClicked(_ sender: UIButton) {
        // shoe cancel ride reason view controller
        let strbd = staticClass.getStoryboard_Map()
        let vc = strbd?.instantiateViewController(withIdentifier: "CancelRideReasonViewController") as! CancelRideReasonViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func getLocationButtonTapped(_ sender: Any) {
        
        let currentLocation = MapViewController.userCurrentLocationMarker.position
        
        if(currentLocation.latitude != 0.0 ){
            
            let camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude,
                                                  longitude: currentLocation.longitude, zoom: 15)
            
            if mapView.isHidden {
                mapView.isHidden = false
                mapView.camera = camera
            } else {
                mapView.animate(to: camera)
            }
        }
        
    }
    
    
    
    
    //MARK:- Notification Observer
    
    // Show All Cars
    @objc func btnHeadClicked(){
        
        
        if(MapViewController.isSelectCarTableViewIsOpen){
            // hide table
            showChoosedCarDetail()
            return
        }
        
        //        MapViewController.isSelectCarTableViewIsOpen = true
        if MainHomeTableVC.availableCarsForeRide.count > 0{
            MapViewController.isSelectCarTableViewIsOpen = true
            self.NSLayoutHeightView.constant = 325
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    // show data piker // notification
    @objc func showDateAndTimePiker(_ notification: NSNotification){
        if let id = notification.userInfo?["id"] as? String {
            
            switch  id {
            case "D" :
                // pike date
                
                if(isRideLaterSelected){
                    isWorkingOnTime = false
                    dateAndTimePikerBackgroundView.isHidden = false
                    dateAndTimePiker.datePickerMode = .date
                }
                else{
                    showToast(message: "You Select Ride Now So You Can't select Date", seconds: 2)
                }
                
                break
                
            case "T" :
                // pike time
                if(isRideLaterSelected){
                    isWorkingOnTime = true
                    dateAndTimePikerBackgroundView.isHidden = false
                    dateAndTimePiker.locale = Locale(identifier: "en_GB")
                    dateAndTimePiker.datePickerMode = .time
                }else{
                    showToast(message: "You Select Ride Now So You Can't select Time", seconds: 2)
                }
                
                break
                
                
            case "P" :
                // show payment
                //show user profile vc
                let strbd = UIStoryboard(name: "Main", bundle: nil)
                let vc = strbd.instantiateViewController(withIdentifier: "PaymentMethodsVC") as! PaymentMethodsVC
                // come from tabel
                vc.fromChoosePayType = true
                self.navigationController?.pushViewController(vc, animated: true)
                
                break
                
                
            default:
                print("")
            }
            
            
            print(id)
            
        }
    }
    
    
    
    // apply coupon code // notification
    @objc func applyCoupnCode(_ notification: NSNotification){
        
        if MapViewController.isPromoApplied{
            MapViewController.isPromoApplied = false
            MapViewController.coupanCode = ""
            NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
            return
        }
        
        
        
        if let couponCode = notification.userInfo?["couponCode"] as? String {
            
            if couponCode == ""{
                self.showToast(message: "Please enter a CouponCode ", seconds: 2)
                return
            }
            
            MapViewController.coupanCode = couponCode
            
            //            Hud.shared.showHud()
            ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
            
            let dictKeys = [ "code" : couponCode,
                             "userID" : global.userId ,
                             //"amount"  : MapViewController.selectedRideMaxAmount
                             "min_amount" : MapViewController.selectedRideMinAmount,
                             "max_amount":MapViewController.selectedRideMaxAmount
            ] as [String : Any]
            
            let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.applypromocode.rawValue//Constants.ApiUrl.applypromocode.rawValue
            
            Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
                //                Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                do {
                    let json: JSON = try JSON(data: response.data!)
                    
                    if (json["statusCode"].stringValue == "200"){
                        
                        //                        MapViewController.selectedRideMaxAmount = json["newprice"].stringValue
                        MapViewController.selectedRideAmountAfterPromo = json["newprice"].stringValue
                        MapViewController.isPromoApplied = true
                        self.view.endEditing(true)
                        
                        //                        self.showToast(message: json["message"].stringValue, seconds: 2)
                        
                        let strbd = staticClass.getStoryboard_Settings()
                        let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                        vc.strTitle = "Thank You"
                        vc.strDescription = "Congratulations! Promo Code has successfully applied."
                        self.present(vc, animated: true, completion: nil)
                        
                        
                        
                        //                          self.showToast(message: "Coupon Code Apply Successfully ", seconds: 2)
                        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                        //  NotificationCenter.default.post(Notification(name: Notification.Name("scrollToTop")))
                        
                    }
                    else{
                        // error
                        self.view.endEditing(true)
                        //                        self.showToast(message: json["message"].stringValue, seconds: 2)
                        //                        self.showToastNew(message: json["message"].stringValue, font: UIFont.systemFont(ofSize: 14.0))
                        //                        self.showCustomPopUp(lbltext: json["message"].stringValue, img: UIImage(named: "information")!, bgColor: .red)
                        
                        self.showCustomPopUp(lbltext: "Promo Code is invalid or expired", img: UIImage(named: "information")!, bgColor: .red)
                        
                        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                        // NotificationCenter.default.post(Notification(name: Notification.Name("scrollToTop")))
                    }
                    
                } catch {
                    // show a alert
                    self.view.endEditing(true)
                    //                    Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                    self.showToast(message: "Please check network connection ", seconds: 2)
                    NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                    //     NotificationCenter.default.post(Notification(name: Notification.Name("scrollToTop")))
                    
                }
            })
        }
        
        
    }
    
    
    private func API_sessionDel(completion:@escaping(Bool) -> ()){
        
        //               Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        let url = Constants.AppLinks.session_Delete_Url + "twiCall/session_delete.php"//Constants.ApiUrl.sessionDel.rawValue
        let dictKeys = [ "bookingId" : MapViewController.AppData["bookingId"] ?? ""]
        
        Alamofire.request(url , method: .post, parameters: dictKeys , headers:nil).responseJSON(completionHandler: { (response) in
            //                   Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                
                if (json["statusCode"].stringValue == "200"){
                    
                    //                    self.cancelRideReasonData =  json["data"]
                    //                    self.cancelRideReasonTableView.reloadData()
                    completion(true)
                }
                else{
                    // error
                    completion(true) //Discussed with vishal
                    //                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                       Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    
    
    //car selection
    @objc func showChoosedCarDetail(){
        
        MapViewController.isSelectCarTableViewIsOpen = false
        
        MapViewController.mapVcViewState = 0
        viewReady.isHidden = false
        viewConfirm.isHidden = true
        viewCallDriver.isHidden = true
        
        self.NSLayoutHeightView.constant = 85
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @objc func showDriverDetail(){
        
        MapViewController.mapVcViewState = 2
        viewReady.isHidden = true
        viewConfirm.isHidden = true
        viewCallDriver.isHidden = false
        
        self.NSLayoutHeightView.constant = 110
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    func showTimeMoneyDetail(){
        
        //        dateAndTimePiker.minimumDate = Date()
        //        print("date picker time is ", dateAndTimePiker.date)
        //        print("minute", Calendar.current.component(.minute, from: Date()))
        ////        print("date is ", Calendar.current.component(.da, from: Date()))
        //
        //        dateAndTimePiker.locale = Locale(identifier: "en_GB")
        getCurrentDateAndTime()
        print("current time is",MapViewController.time)
        viewReady.isHidden = true
        viewConfirm.isHidden = false
        viewCallDriver.isHidden = true
        
        MapViewController.mapVcViewState = 1
        self.NSLayoutHeightView.constant = 160
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    
    @objc private func resetMapViewController(){
        
        //  DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        
        let nilLocation = CLLocationCoordinate2DMake( 0, 0)
        
        
        MapViewController.isWorkingOnUserRide = true
        MapViewController.isDriverPathCreated = false
        MapViewController.isUserPathCreated = false
        MapViewController.isPromoApplied = false
        MapViewController.coupanCode = ""
        
        MapViewController.selectedSourceLocation = nilLocation
        MapViewController.selectedDestinationLocation = nilLocation
        MapViewController.selectedSourceLocationName = ""
        MapViewController.selectedDestinationLocationName = ""
        
        
        MapViewController.selectedCarIndex = -1
        MapViewController.mapVcViewState = 0
        MainHomeTableVC.availableCarsForeRide = []
        //            MapViewController.paymentType  = "Payment"
        // UserDefaults.standard.set("Payment", forKey: "PAYMENT")
        
        MapViewController.paymentCardId = ""
        self.showChoosedCarDetail()
        
        MainHomeTableVC.isNextAvailable = true
        MainHomeTableVC.isDriverDetailShow = false
        MainHomeTableVC.isTimeMoneyCellShow = false
        self.manageViewStage()
        MapViewController.selectedRideMaxAmount = "0"
        MapViewController.selectedRideAmountAfterPromo = "0"
        MapViewController.selectedRideMinAmount = "0"
        self.manageMapElements()
        
        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
        //  }
    }
    
    // MARK:- textfield delegat func
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if MapViewController.userCanCreateNewRide{
            
            if textField == txtPickUpLocation || textField == txtDestination{
                
                resetMapViewController()
                
                let strbd = staticClass.getStoryboard_Map()
                let vc = strbd?.instantiateViewController(withIdentifier: "SelectDestinationVC") as! SelectDestinationVC
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        }
        
        return false
    }
    
    
    
    // MARK:- date paker
    
    
    //action
    
    @IBAction func datePikerHideOrCancelButtonTapped(_ sender: Any) {
        dateAndTimePikerBackgroundView.isHidden = true
    }
    
    
    @IBAction func datePikerOkButtonTapped(_ sender: Any) {
        
        if(isWorkingOnTime){
            let timeFormatter = DateFormatter()
            //timeFormatter.timeStyle = DateFormatter.Style.short
            timeFormatter.dateFormat = "HH:mm"
            let strDate = timeFormatter.string(from: dateAndTimePiker.date)
            
            MapViewController.time = strDate
        }
        else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let selectedDate = dateFormatter.string(from: dateAndTimePiker.date)
            MapViewController.date = selectedDate
        }
        
        
        dateAndTimePikerBackgroundView.isHidden = true
        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
    }
    
    
    
    
    
    //MARK:- LOCATION deleget function
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied, .notDetermined:
            showPermissionAlert()
        case .authorizedAlways:
            
            print("Location status not determined.")
            
        case .authorizedWhenInUse:
            print("Location status is OK.")
            
        default:
            fatalError()
        }
    }
    
    
    func showPermissionAlert(){
        
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        alertController.addAction(cancelAction)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location: CLLocation = locations.last!
        
        //        print("======================================================================================")
        //        print(location)
        
        // update user current location
        MapViewController.userCurrentLocationMarker.position = location.coordinate
        
        
        print(AppDelegate.appCurrentState)
        /*
         if(self.txtPickUpLocation.text == "" && AppDelegate.appCurrentState  ==  1 && MapViewController.isGiocondaApiHit == false){
         
         
         if(location.coordinate.longitude != 0.0){
         
         MapViewController.isGiocondaApiHit = true
         
         let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
         longitude: location.coordinate.longitude, zoom: 15)
         
         if mapView.isHidden {
         mapView.isHidden = false
         mapView.camera = camera
         } else {
         mapView.animate(to: camera)
         }
         
         getUserCurrentLocationNameToLat_Lng(location: location.coordinate)
         
         }
         }
         */
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //MARK:- make marker on goole map
    
    func makeCarMarkOnMap(){
        
        // remove all car  marker to map
        for i in 0..<MapViewController.availableCarsMarkerArray.count{
            MapViewController.availableCarsMarkerArray[i].map = nil
        }
        // remove all data from array
        if(MapViewController.availableCarsMarkerArray.count > 0){
            MapViewController.availableCarsMarkerArray.removeAll()
        }
        
        
        let carMarkerImage = UIImage(named: "white_taxi")!.withRenderingMode(.alwaysOriginal)
        let carMarkerView = UIImageView(image: carMarkerImage)
        carMarkerView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        
        
        //   TODO:- make marker and add in array
        
        for i in 0..<carsAvailableNearUser.count{
            
            let carMarker = GMSMarker()
            MapViewController.availableCarsMarkerArray.append(carMarker)
            
            let car = carsAvailableNearUser[i]
            
            let carLat : Double = car["latitude"].doubleValue
            let carLng : Double  = car["longlatitude"].doubleValue
            let location = CLLocationCoordinate2DMake(carLat , carLng )
            
            carMarker.iconView = carMarkerView
            carMarker.map = mapView
            mapView.selectedMarker = carMarker
            carMarker.position = location
            
            MapViewController.availableCarsMarkerArray.append(carMarker)
            
        }
        
    }
    
    
    //MARK:- Google MAP Apis
    
    
    //get place name using lat long // goole map api(reverce giocoding)
    func getUserCurrentLocationNameToLat_Lng(location : CLLocationCoordinate2D ){
        
        print("======================================================================================")
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.latitude),\(location.longitude)&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        
        let urlEncoded =  url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        
        
        Alamofire.request( urlEncoded , method: .get , headers : nil).responseJSON(completionHandler: { (response) in
            
            
            
            if(response.result.isSuccess){
                
                let jsondata :JSON = JSON(response.data)
                
                
                // set address in pickuplocation textfield
                
                let strCurrentLocation = jsondata["results"][0]["formatted_address"].stringValue
                
                if strCurrentLocation != "" {
                    
                    MapViewController.sourceLocation = location
                    MapViewController.sourceLocationNameTemp = strCurrentLocation
                    
                    //                    MapViewController.selectedSourceLocation = location
                    //                    MapViewController.selectedSourceLocationName = strCurrentLocation
                    
                    self.txtPickUpLocation.text = strCurrentLocation
                    print("current address from deepak code", strCurrentLocation)
                    
                }
                
            }
        })
    }
    
    
    //MARK:- REVERSE GEO CODE API
    
    func API_ReverseGeoCoding(completion:@escaping(Bool)->()){
        
        if locationManager.location == nil{return}
        CLGeocoder().reverseGeocodeLocation(locationManager.location!, completionHandler: {(placemarks, error)-> Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                completion(false)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark
                
                let camera = GMSCameraPosition.camera(withLatitude: self.locationManager.location!.coordinate.latitude,
                                                      longitude: self.locationManager.location!.coordinate.longitude, zoom: 15)
                
                if self.mapView.isHidden {
                    self.mapView.isHidden = false
                    self.mapView.camera = camera
                } else {
                    self.mapView.animate(to: camera)
                }
                
                //                   self.displayLocationInfo(placemark: pm)
                self.displayLocationInfo(placemark: pm) { (success) in
                    if success{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
                
            } else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark,completion:@escaping(Bool) -> ()) {
        
        //stop updating location to save battery life
        //        locationManager.stopUpdatingLocation()
        //            print((placemark.locality != nil) ? placemark.locality : "")
        //            print((placemark.postalCode != nil) ? placemark.postalCode : "")
        //            print((placemark.administrativeArea != nil) ? placemark.administrativeArea : "")
        //            print((placemark.country != nil) ? placemark.country : "")
        //            print(placemark.subLocality)
        //            print(placemark.name)
        //            print(placemark.region)
        
        let lat = placemark.location!.coordinate.latitude
        let lng = placemark.location!.coordinate.longitude
        
        let service = Geocoder.Google(lat:lat , lng: lng, APIKey: Constants.AppLinks.APIKEY_Google)
        
        SwiftLocation.geocodeWith(service).then { result in
            // Different services, same expected output [GeoLocation]
            print(result.data)
            guard let geolocation = result.data?.first else{return}
            guard let formattedAddress = geolocation.info[.formattedAddress] else{return}
            
            
            MapViewController.sourceLocation = placemark.location!.coordinate
            //            swiftLocation(lat: placemark.location!.coordinate.latitude, lng: placemark.location!.coordinate.longitude)
            MapViewController.sourceLocationNameTemp = formattedAddress ?? ""
            MapViewController.pincode = placemark.postalCode ?? ""
            self.txtPickUpLocation.text = formattedAddress
            completion(true)
            
        }
        
        
        
        //        let name = placemark.name ?? ""
        //        let subLocality = placemark.subLocality ?? ""
        //        let locality = placemark.locality ?? ""
        //        let administrativeArea = placemark.administrativeArea ?? ""
        //        let country = placemark.country ?? ""
        //        let postalCode = placemark.postalCode ?? ""
        //
        //
        //
        //
        //        let currentAddress = "\(name), \(subLocality), \(locality), \(administrativeArea), \(country), \(postalCode)"
        //        print("current address from geo code is",currentAddress)
        //
        
        
        
        
        
    }
    
    
    
    
    // get path from direction api(google map api)
    func fetchRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D , isPathForUser : Bool) {
        
        // let test: CLLocationCoordinate2D = CLLocationCoordinate2DMake(100, 100)
        
        let session = URLSession.shared
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o")!
        //AIzaSyBPdAWhGw-hnn4yW7_9OzkjhIqYSWzdN2M
        //"https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=API_KEY"
        
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            guard let jsonResult = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] else {
                print("error in JSONSerialization")
                return
            }
            
            
            
            guard let routes = jsonResult["routes"] as? [Any] else {
                return
            }
            
            if(routes.count <= 0){
                return
            }
            
            guard let route = routes[0] as? [String: Any] else {
                return
            }
            
            guard let overview_polyline = route["overview_polyline"] as? [String: Any] else {
                return
            }
            
            guard let polyLineString = overview_polyline["points"] as? String else {
                return
            }
            
            //Call this method to draw path on map
            self.drawPath(from: polyLineString, a: source, b: destination , isPathForUser: isPathForUser)
            
        })
        task.resume()
    }
    
    //draw path function
    func drawPath(from polyStr: String , a : CLLocationCoordinate2D,  b : CLLocationCoordinate2D , isPathForUser : Bool){
        
        DispatchQueue.main.async()
        {
            let path = GMSPath(fromEncodedPath: polyStr)
            
            MapViewController.userPickUpLocationMarker.map = nil
            MapViewController.userDropLocationMarker.map = nil
            
            //                MapViewController.driverStartLocationMarker.map = nil
            MapViewController.driverEndLocationMarker.map = nil
            
            
            MapViewController.polyline.map = nil
            MapViewController.polylineForDriver.map = nil
            
            // Google MapView show path
            if (isPathForUser) {
                
                MapViewController.userPickUpLocationMarker.map = self.mapView
                MapViewController.userPickUpLocationMarker.position = a
                
                if MapViewController.driverCurrentData["status"].stringValue == "Start"{
                    MapViewController.userPickUpLocationMarker.map = nil
                    self.changeMarkerImage(imageName: "source_marker_1")
                }else{
                    self.changeMarkerImage(imageName: "destination_marker_1")
                }
                
                MapViewController.userDropLocationMarker.map = self.mapView
                MapViewController.userDropLocationMarker.position = b
                
                
                
                MapViewController.polyline = GMSPolyline(path: path)
                MapViewController.polyline.strokeWidth = 3.0
                MapViewController.polyline.strokeColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)
                MapViewController.polyline.map = self.mapView
                MapViewController.isUserPathCreated = true
                self.getAllPathLocationCoordinates(forUser: true)
            }
            else{
                
                
                //                    MapViewController.driverStartLocationMarker.map = self.mapView
                //                    MapViewController.driverStartLocationMarker.position = a
                
                MapViewController.driverEndLocationMarker.map = self.mapView
                MapViewController.driverEndLocationMarker.position = b
                
                MapViewController.polylineForDriver = GMSPolyline(path: path)
                MapViewController.polylineForDriver.strokeWidth = 3.0
                MapViewController.polylineForDriver.strokeColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)//#colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
                MapViewController.polylineForDriver.map = self.mapView
                MapViewController.isDriverPathCreated = true
                
                self.getAllPathLocationCoordinates(forUser: false)
                
                
            }
        }
    }
    
    
    //MARK:- LOCAL APIS
    
    func getAllPathLocationCoordinates(forUser : Bool){
        
        MapViewController.testPathCoordinatesArray.removeAll()
        
        var index : UInt = 0
        
        if(forUser){
            
            MapViewController.userPathCoordinatesArray.removeAll()
            
            while (  MapViewController.polyline.path?.coordinate(at: index) != nil  && (MapViewController.polyline.path?.coordinate(at: index).latitude != -180.0 || MapViewController.polyline.path?.coordinate(at: index).longitude != -180.0) ) {
                
                if let coordinate = MapViewController.polyline.path?.coordinate(at: index){
                    
                    MapViewController.testPathCoordinatesArray.append(coordinate)
                    
                    let lat =  (coordinate.latitude * 10000).rounded()/10000
                    let long =  (coordinate.longitude * 10000).rounded()/10000
                    
                    let tempC  = CLLocationCoordinate2DMake(lat, long)
                    MapViewController.userPathCoordinatesArray.append(tempC)
                    index = index + 1
                }
            }
            
        }
        else{
            
            MapViewController.driverPathCoordinatesArray.removeAll()
            
            while (  MapViewController.polylineForDriver.path?.coordinate(at: index) != nil && (MapViewController.polylineForDriver.path?.coordinate(at: index).latitude != -180.0 || MapViewController.polylineForDriver.path?.coordinate(at: index).longitude != -180.0) ) {
                
                if let coordinate = MapViewController.polylineForDriver.path?.coordinate(at: index){
                    
                    MapViewController.testPathCoordinatesArray.append(coordinate)
                    
                    let lat =  (coordinate.latitude * 10000).rounded()/10000
                    let long =  (coordinate.longitude * 10000).rounded()/10000
                    
                    let tempC  = CLLocationCoordinate2DMake(lat, long)
                    MapViewController.driverPathCoordinatesArray.append(tempC)
                    index = index + 1
                }
            }
            
            
        }
        
    }
    
    
    func showToastForBooking(){
        
        self.lblToast.text = "Thank you for choosing Infinite Cabs\nPlease wait while we are searching a closest driver for you"//json["message"].stringValue//
        
        
        self.imgTick.image = UIImage(named: "tick")
        self.viewToast.backgroundColor = UIColor(red: 76/255, green: 176/255, blue: 80/255, alpha: 1)
        
        
        
        DispatchQueue.main.async {
            
            self.view.bringSubviewToFront(self.viewToast)
            
            UIView.animate(withDuration: 2, delay: 0, options: .transitionFlipFromBottom, animations: {
                self.viewToast.isHidden = false
            }){ (completd) in
                self.viewToast.layer.cornerRadius = self.viewToast.bounds.height/2
            }
            
        }
    }
    
    
    private func searchDriverAPI(){
        
        //        Hud.shared.showHud()
        //   ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        
        
        let dictKeys = [ "booking_id" : MapViewController.AppData["bookingId"] ?? "",
                         "pick" : MapViewController.selectedSourceLocationName ,
                         "drop" : MapViewController.selectedDestinationLocationName ,
                         "user_latitude" : MapViewController.selectedSourceLocation.latitude ,
                         "user_longlatitude" : MapViewController.selectedSourceLocation.longitude
                         
        ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.searchDriver.rawValue
        
        print("param",dictKeys)
        print("url",url)
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            
            
            do {
                
                let json: JSON = try JSON(data: response.data!)
                print("json is",json)
                print("code is",json["statusCode"].stringValue )
                
                if (json["statusCode"].stringValue == "200"){
                    
                    self.timeCount = 0
                    
                    MapViewController.coupanCode = ""
                    
                    //                    ActivityIndicatorWithLabel.shared.hideProgressView()
                    
                    MapViewController.AppData.updateValue(json["bookingData"]["driverId"].stringValue, forKey: "driverId" )
                    
                    
                    MapViewController.AppData.updateValue(json["bookingData"]["driverName"].stringValue, forKey: "driverName" )
                    MapViewController.AppData.updateValue(json["bookingData"]["userstarrating"].stringValue, forKey: "driverRating" )
                    
                    
                    MapViewController.AppData.updateValue(json["bookingData"]["drivercar_no"].stringValue, forKey: "carNumber" )
                    MapViewController.AppData.updateValue(json["bookingData"]["driverCar_Model"].stringValue, forKey: "carName" )
                    
                    
                    //TODO//carimage
                    //   MapViewController.AppData.updateValue(json["bookingData"]["driverId"].stringValue, forKey: "carImage" )
                    MapViewController.AppData.updateValue(json["bookingData"]["driverImage"].stringValue, forKey: "deiverImage" )
                    
                    
                    
                    //deepak_todo
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ShowDriverDetail")))
                    NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                    
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "DismissWaitAndCancelRideVC")))
                    
                    /*  DispatchQueue.main.async {
                     
                     UIView.animate(withDuration: 2, delay: 0, options: .transitionFlipFromTop, animations: {
                     self.viewToast.isHidden = true
                     }, completion: nil)
                     
                     }
                     */
                    self.getDriverCurrentData()
                    
                }
                else if (json["statusCode"].stringValue == "201") || (json["statusCode"].stringValue == "202"){
                    
                    //                    if self.timeCount >= 300{
                    //                        self.timeCount = 0
                    
                    //                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "DismissWaitAndCancelRideVC")))
                    
                    /*  DispatchQueue.main.async {
                     UIView.animate(withDuration: 2, delay: 0, options: .transitionFlipFromTop, animations: {
                     self.viewToast.isHidden = true
                     }, completion: nil)
                     }
                     
                     //                        self.viewToast.isHidden = true
                     ActivityIndicatorWithLabel.shared.hideProgressView()
                     */
                    
                    //                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    //                            DispatchQueue.main.async {
                    //                                self.resetMapViewController()
                    
                    //                        if self.timeCount >= 500{
                    //                            self.timeCount = 0
                    //                            return
                    //                        }
                    /*      let strbd = staticClass.getStoryboard_Settings()
                     let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                     vc.strTitle = "We are very sorry"
                     vc.strDescription = "No driver is currently\n available in the area"
                     vc.strBtnOkTitle = "OK"
                     vc.btnOkWidth = 80
                     //                                vc.delegate = self
                     self.present(vc, animated: true, completion: nil)
                     
                     
                     self.timeCount = 0
                     return*/
                    //                            }
                    //                        }
                    
                    
                    
                    //                    }
                    
                    //                    self.view.bringSubviewToFront(self.viewToast)
                    
                    
                    
                    
                    //hit agen same api
                    
                    //Hud.shared.showHudWithMsg(json["message"].stringValue)
                    
                    //                    self.showToast(message: json["message"].stringValue, seconds: 4)
                    //
                    //                    DispatchQueue.main.async {
                    //                        let strbd = staticClass.getStoryboard_Settings()
                    //                        let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                    //                        vc.strTitle = "Thank you for choosing Infinite Cabs"
                    //                        vc.strDescription = "Please wait while we are searching a closest driver for you."
                    //                        vc.imgPop = #imageLiteral(resourceName: "checkMark")
                    //                        //                    vc.strBtnOkTitle = "Yes"
                    //                        self.present(vc, animated: true, completion: nil)
                    //
                    //                    }
                    
                    
                    
                    /*
                     self.lblToast.text = "Thank you for choosing Infinite Cabs\nPlease wait while we are searching a closest driver for you"//json["message"].stringValue//
                     
                     
                     self.imgTick.image = UIImage(named: "tick")
                     self.viewToast.backgroundColor = UIColor(red: 76/255, green: 176/255, blue: 80/255, alpha: 1)
                     
                     
                     
                     DispatchQueue.main.async {
                     
                     self.view.bringSubviewToFront(self.viewToast)
                     
                     UIView.animate(withDuration: 2, delay: 0, options: .transitionFlipFromBottom, animations: {
                     self.viewToast.isHidden = false
                     }){ (completd) in
                     self.viewToast.layer.cornerRadius = self.viewToast.bounds.height/2
                     }
                     
                     }*/
                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + self.callBackTime) {
                    //                        self.searchDriverAPI()
                    //                    }
                    
                    
                    if self.isCanceled{return}
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + self.callBackTime) {
                        DispatchQueue.global(qos: .background).async {
                            self.searchDriverAPI()
                        }
                    }
                    
                    
                    //                    DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + self.callBackTime) {
                    //                        self.searchDriverAPI()
                    //                    }
                    
                }
                else if (json["statusCode"].stringValue == "203") || (json["statusCode"].stringValue == "206") {
                    
                    let strbd = staticClass.getStoryboard_Settings()
                    let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                    vc.strTitle = "We are very sorry"
                    vc.strDescription = "No driver is currently\n available in the area"
                    vc.strBtnOkTitle = "OK"
                    vc.btnOkWidth = 80
                    //                                vc.delegate = self
                    self.present(vc, animated: true, completion: nil)
                    
                    self.timeCount = 0
                    return
                    
                }
                else{
                    // error
                    self.timeCount = 0
                    
                    /*
                     DispatchQueue.main.async {
                     UIView.animate(withDuration: 2, delay: 0, options: .transitionFlipFromTop, animations: {
                     self.viewToast.isHidden = true
                     }, completion: nil)
                     }
                     ActivityIndicatorWithLabel.shared.hideProgressView()
                     */
                    
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "DismissWaitAndCancelRideVC")))
                    
                    
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                    self.resetMapViewController()
                }
                
            } catch {
                self.timeCount = 0
                // show a alert
                //                Hud.shared.hideHud()
                //                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    func getAddressFromLatLng(_ lat:CLLocationDegrees, _ lng:CLLocationDegrees, completion:@escaping(Bool)->()){
        
        let service = Geocoder.Google(lat:lat , lng: lng, APIKey: Constants.AppLinks.APIKEY_Google)
        
        SwiftLocation.geocodeWith(service).then { result in
            // Different services, same expected output [GeoLocation]
            print(result.data)
            guard let geolocation = result.data?.first else{return  completion(false)}
            guard let pin = geolocation.info[.postalCode] else{return  completion(false)}
            
            MapViewController.pincode = pin ?? ""
            
            //            MapViewController.pincode = placemark.postalCode ?? ""
            
            completion(true)
            
        }
        
    }
    
    
    
    // first first first
    @objc private func boookingCabAPI(){
        
        
        getAddressFromLatLng(MapViewController.selectedSourceLocation.latitude, MapViewController.selectedSourceLocation.longitude) { (success) in
            
            if success{
                self.bookCabApi()
            }
            
        }
        
        
    }
    
    
    func bookCabApi(){
        
        self.isCanceled = false
        
        //       Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
        //        self.showToastForBooking()
        let time =  MapViewController.date + " " + MapViewController.time
        
        let userID = global.userId
        
        var bookingNowOrLater = "now"
        
        if(isRideLaterSelected){
            bookingNowOrLater = "booklater"
        }
        
        var paymentStatus = UserDefaults.standard.value(forKey: "PAYMENT") as! String
        
        if paymentStatus == "Pay the Driver Directly"{
            paymentStatus = "cash"
        }else if paymentStatus == "Pay with the App"{
            paymentStatus = "card"
        }
        
        var final_amount = ""
        
        if MapViewController.isPromoApplied{
            final_amount = MapViewController.selectedRideAmountAfterPromo
        }else{
            final_amount = MapViewController.selectedRideMaxAmount
        }
        
        
        
        let cardID = UserDefaults.standard.value(forKey: "CARDID") as! String
        
        
        let dictKeys = [
            
            "user_id" : userID,
            "cabType" : MapViewController.AppData["cabId"] ?? "",
            "cab_id" : MapViewController.AppData["cabId"] ?? "",//selectedCabId,
            "pickup_lat" : MapViewController.selectedSourceLocation.latitude,
            "pickup_long" : MapViewController.selectedSourceLocation.longitude,
            "drop_lat" : MapViewController.selectedDestinationLocation.latitude,
            "drop_long" :  MapViewController.selectedDestinationLocation.longitude,
            "isdevice" : "ios",
            "payment_type" : paymentStatus,
            "message" :  MapViewController.noteForDriver,
            "booking_type" : bookingNowOrLater  ,
            "book_create_date_time" : time ,
            "amount" :    final_amount ,
            "pincode" : MapViewController.pincode ,
            "coupan":MapViewController.coupanCode,
            "cardId" : cardID ,
            "drop_address" : MapViewController.selectedDestinationLocationName ,
            "pick_address" : MapViewController.selectedSourceLocationName,
            "pickup_date_time" : time
            
        ] as [String : Any]
        
        print("Book ride param", dictKeys)
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.boookingCab.rawValue
            
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //              Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                print("Book ride json response", json["data"])
                
                if (json["statusCode"].stringValue == "200"){
                    
                    MapViewController.AppData.updateValue(json["data"]["cab_id"].stringValue, forKey: "cabId")
                    MapViewController.AppData.updateValue(json["data"]["booking_id"].stringValue, forKey: "bookingId")
                    
                    
                    //                    showCustomPopUp(lbltext: T##String, img: <#T##UIImage#>, bgColor: <#T##UIColor#>)
                    //                    self.showToastForBooking()
                    //                    ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
                    //                    self.showToastForBooking()
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimerFires), userInfo: nil, repeats: true)
                    
                    self.searchDriverAPI()
                    DispatchQueue.main.async {
                        self.showRideWaitOrCancelPopUp()
                    }
                    
                    
                    //     NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
                    
                } else if (json["statusCode"].stringValue == "201"){
                    
                    let strbd = staticClass.getStoryboard_Settings()
                    let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                    vc.strTitle = "Thank you for choosing Infinite Cabs"
                    vc.strDescription = json["message"].stringValue
                    vc.imgPop = #imageLiteral(resourceName: "girl")
                    self.present(vc, animated: true, completion: nil)
                    
                    self.resetMapViewController()
                    
                }
                else if (json["statusCode"].stringValue == "202"){
                    //                    self.showAlert(title: json["message"].stringValue, message: "")
                    
                    let strbd = staticClass.getStoryboard_Settings()
                    let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                    vc.strTitle = "Thank you for choosing Infinite Cabs"
                    vc.strDescription = "Your booking has successfully been received and an available cab will be sent to your pick up address."
                    //                    vc.imgPop = #imageLiteral(resourceName: "checkMark")
                    //                    vc.strBtnOkTitle = "Yes"
                    self.present(vc, animated: true, completion: nil)
                    
                    
                    self.resetMapViewController()
                }
                else{
                    // error
                    self.showCustomPopUp(lbltext: json["message"].stringValue, img: UIImage(named: "information")!, bgColor: .red)
                    //                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //                Hud.shared.hideHud()
                debugPrint(error.localizedDescription)
                ActivityIndicatorWithLabel.shared.hideProgressView()
                self.showAlert(title: "Error", message: "Please check network connection")
            }
        })
    }
    
    
    
    
    
    @objc func onTimerFires()
    {
        timeCount += 1
        
        if timeCount >= 300 {
            timer?.invalidate()
            timer = nil
        }
    }
    
    private func getAllCarDataAvailableNearUserAPI(){
        
        let userCurrentLocation = MapViewController.userCurrentLocationMarker.position
        
        
        if( (userCurrentLocation.latitude != -180.0 || userCurrentLocation.longitude != -180.0) && (userCurrentLocation.latitude != 0.0 || userCurrentLocation.longitude != 0.0) )
        {
            // get location
            
            // get car data
            let strLat : String = "\(userCurrentLocation.latitude)"
            let strLong : String = "\(userCurrentLocation.longitude)"
            
            let dictKeys = [ "lat" : strLat,  "lng" : strLong ] as [String:String]
            
            let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.serchUserTexi.rawValue
            
            Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
                
                //    Hud.shared.hideHud()
                do {
                    let json: JSON = try JSON(data: response.data!)
                    
                    if (json["statusCode"].stringValue == "200"){
                        
                        self.carsAvailableNearUser = json["data"]
                        self.makeCarMarkOnMap()
                    }
                    else{
                        // error
                        self.showAlert(title: "Error", message: json["message"].stringValue)
                    }
                    
                } catch {
                    // show a alert
                    //                    Hud.shared.hideHud()
                    ActivityIndicatorWithLabel.shared.hideProgressView()
                    self.showAlert(title: "Error", message: "Please check network connection")
                }
            })
            
        }
        else{
            // create a thread and run again same function
            DispatchQueue.main.asyncAfter(deadline: .now() + callBackTime) {
                self.getAllCarDataAvailableNearUserAPI()
            }
            
        }
        
    }
    
    
    func showPaymentAlert(){
        
        let alert = UIAlertController(title: "Infinite Cabs", message: "Do you want to do payment", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Make Payment", style: .default, handler: { (UIAlertAction) in
            // show reating view controller
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (UIAlertAction) in
            // call agan same api after 30 second // callbacktime
            DispatchQueue.main.asyncAfter(deadline: .now() + self.callBackTime) {
                self.getDriverCurrentData()
            }
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    private func getDriverCurrentData(){
        
        //        Hud.shared.showHud()
        let userID = global.userId
        
        
        let dictKeys = [ "user_id" : userID,
                         "driver_id" : MapViewController.AppData["driverId"] ?? "" ,//MapViewController.selectedDriverID,
                         "booking_id" :  MapViewController.AppData["bookingId"] ?? ""
        ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.get_user_driver_latlong.rawValue//Constants.ApiUrl.get_user_driver_latlong.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            //            Hud.shared.hideHud()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    MapViewController.driverCurrentData = json["data"]
                    self.manageRider()
                    
                }
                else{
                    // error
                    
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //               Hud.shared.hideHud()
                self.showAlert(title: "Error", message: "Please check network connection")
                
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + self.callBackTime * 3) {
                    self.getDriverCurrentData()
                }
            }
        })
    }
    
    
    
    
    
    private func getDriverMobileNumber(){
        
        //        Hud.shared.showHud()
        let userID = global.userId
        
        
        let dictKeys = [
            "user_id" : userID
            
        ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + Constants.ApiUrl.getDriverMobileNumber.rawValue//Constants.ApiUrl.getDriverMobileNumber.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            //            Hud.shared.hideHud()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    let jsondata = json["bookingData"]
                    //                        MapViewController.driverMobileNumber = jsondata["twilio"].stringValue
                    let mobilenumber = jsondata["twilio"].stringValue
                    DispatchQueue.main.async {
                        self.callNumber(phoneNumber: mobilenumber)
                    }
                    
                    
                    
                }
                else{
                    // error
                    
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //               Hud.shared.hideHud()
                self.showAlert(title: "Error", message: "Please check network connection")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + self.callBackTime * 3) {
                    self.getDriverCurrentData()
                }
            }
        })
    }
    
    
    
    
    private func manageRider(){
        
        
        if(MapViewController.driverCurrentData["status"].stringValue == "Completed" ){
            // show reating view controller
            
            MapViewController.userCanCreateNewRide = false
            
            //TODO:
            if MapViewController.driverCurrentData["payStatus"].stringValue == "2"{
                
                let strbd = staticClass.getStoryboard_Settings()
                let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                vc.strTitle = "Thank you for using Infinite Cabs"
                vc.strDescription = "Trip Completed\n App payment error occur. Please pay the Cash directly to the Driver"
                vc.strBtnOkTitle = "OK"
                vc.btnOkWidth = 80
                //            vc.imgPop = #imageLiteral(resourceName: "checkMark")
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                
            }else{
                
                let strbd = staticClass.getStoryboard_Settings()
                let vc = strbd?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                vc.strTitle = "Thank you for using Infinite Cabs"
                vc.strDescription = "Your ride has successfully completed. Please rate your driver now."
                vc.strBtnOkTitle = "Driver Rating"
                vc.btnOkWidth = 140
                //            vc.imgPop = #imageLiteral(resourceName: "checkMark")
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                
            }
            
            
        }
        else if(MapViewController.driverCurrentData["status"].stringValue == "Driver_Declined" ){
            
            //  showAlert(title: "Driver Cancel Ride ", message: "")
             searchDriverAPI()
            DispatchQueue.main.async {
                self.showRideWaitOrCancelPopUp()
            }
            
        }
        
        else if(MapViewController.driverCurrentData["status"].stringValue == "paymentPending" ){
            
            //TODO:
            
            //Please wait processing payment
            
            // dialog
            MapViewController.userCanCreateNewRide = false
            
            if(MapViewController.driverCurrentData["paymentType"].stringValue == "Cash"){
                // cash mode is selected
                DispatchQueue.main.asyncAfter(deadline: .now() + callBackTime) {
                    self.getDriverCurrentData()
                }
            }
            else{
                
                Hud.shared.showHudWithMsg("Please wait processing payment")
                //                showPaymentAlert()
            }
            
        }
        
        
        else if(MapViewController.driverCurrentData["status"].stringValue == "Accepted" ){
            MapViewController.userCanCreateNewRide = false
            MapViewController.isWorkingOnUserRide = false
            
            manageDriverMapElements()
            
            // call agan same api after 30 second // callbacktime
            DispatchQueue.main.asyncAfter(deadline: .now() + callBackTime) {
                self.getDriverCurrentData()
            }
            
        }
        
        else if(MapViewController.driverCurrentData["status"].stringValue == "Start" ){
            // driver reached
            MapViewController.userCanCreateNewRide = false
            MapViewController.isWorkingOnUserRide = true
            manageDriverMapElements()
            // call agan same api after 30 second // callbacktime
            DispatchQueue.main.asyncAfter(deadline: .now() + callBackTime) {
                self.getDriverCurrentData()
            }
            
        }
    }
    
    
    func manageDriverMapElements(){
        
        let etaTime = MapViewController.driverCurrentData["eta"].stringValue
        MapViewController.AppData.updateValue(etaTime, forKey: "time")
        NotificationCenter.default.post(Notification(name: Notification.Name("ReloadTabel")))
        
        
        let lastlocation = MapViewController.driverCurrentLocationMarker.position
        
        let driverLat = MapViewController.driverCurrentData["latitude"].doubleValue
        let driverLong = MapViewController.driverCurrentData["longlatitude"].doubleValue
        
        let driverCurretLocation = CLLocationCoordinate2DMake(driverLat, driverLong)
        
        carAnimactionAToBCordinates(a:lastlocation , b: driverCurretLocation )
        
        print("driverCurretLocation ==================================")
        print(driverCurretLocation)
        print("driver last location ==================================")
        print(lastlocation)
        
        
        let lat =  (driverLat * 10000).rounded()/10000
        let long =  (driverLong * 10000).rounded()/10000
        
        let deiverCurrenTrimedLocation = CLLocationCoordinate2DMake(lat, long)
        
        
        
        
        if(MapViewController.isWorkingOnUserRide){
            // working on user ride
            // status = "start" // when car riched on user start location
            
            if(MapViewController.isUserPathCreated == false){
                
                // show user pickup and drop location marker
                
                MapViewController.userPickUpLocationMarker.map = mapView
                MapViewController.userDropLocationMarker.map = mapView
                
                if MapViewController.driverCurrentData["status"].stringValue == "Start"{
                    MapViewController.userPickUpLocationMarker.map = nil
                    changeMarkerImage(imageName: "source_marker_1")
                    
                }else{
                    changeMarkerImage(imageName: "destination_marker_1")
                }
                
                MapViewController.userPickUpLocationMarker.position = MapViewController.selectedSourceLocation
                MapViewController.userDropLocationMarker.position = MapViewController.selectedDestinationLocation
                MapViewController.polyline.map = self.mapView
                
                
                // hide driver data
                MapViewController.polylineForDriver.map = nil
                MapViewController.driverEndLocationMarker.map = nil//?.isHidden = true
                //                MapViewController.driverStartLocationMarker.map = nil //?.isHidden = true
                
                
                // and hide user current location marker
                MapViewController.userCurrentLocationMarker.map = nil //?.isHidden = true
                
                fetchRoute(from: driverCurretLocation , to: MapViewController.selectedDestinationLocation, isPathForUser: true)
            }
            
            if MapViewController.driverCurrentData["status"].stringValue == "Start"{
                MapViewController.userPickUpLocationMarker.map = nil
                changeMarkerImage(imageName: "source_marker_1")
                
            }else{
                changeMarkerImage(imageName: "destination_marker_1")
            }
            
            if(MapViewController.userPathCoordinatesArray.count == 0){return}
            
            var isOnPath = false
            var index = 0
            
            for i in 0..<MapViewController.userPathCoordinatesArray.count{
                
                if(MapViewController.userPathCoordinatesArray[i].latitude == deiverCurrenTrimedLocation.latitude && MapViewController.userPathCoordinatesArray[i].longitude == deiverCurrenTrimedLocation.longitude ){
                    
                    isOnPath = true
                    index = i
                    break
                }
                
            }
            
            if(isOnPath){
                print("driver on path")
                
                let newPath = GMSMutablePath()
                
                for i in index..<Int(MapViewController.testPathCoordinatesArray.count){
                    newPath.add(MapViewController.testPathCoordinatesArray[i])
                }
                
                MapViewController.polyline.map = nil
                MapViewController.polyline = GMSPolyline(path: newPath)
                MapViewController.polyline.strokeColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)//UIColor.red
                MapViewController.polyline.strokeWidth = 3.0
                MapViewController.polyline.map = self.mapView
                
                
            }
            else{
                fetchRoute(from: driverCurretLocation , to: MapViewController.selectedDestinationLocation, isPathForUser: true)
            }
            
            
        }
        else{
            // dericer ride
            // driver go to her currinent location to user currient location
            
            // create driver Path
            if(MapViewController.isDriverPathCreated == false){
                
                // hide user data
                // hide user path source to destancinaction
                MapViewController.polyline.map = nil
                // hide user markers all markers
                MapViewController.userPickUpLocationMarker.map = nil
                MapViewController.userDropLocationMarker.map = nil
                
                
                // show driver markers
                MapViewController.driverCurrentLocationMarker.map = mapView
                MapViewController.driverEndLocationMarker.map = mapView
                //                MapViewController.driverStartLocationMarker.map = mapView//?.isHidden = false
                
                
                
                //                MapViewController.driverStartLocationMarker.position = driverCurretLocation
                MapViewController.driverEndLocationMarker.position = MapViewController.selectedSourceLocation
                MapViewController.driverCurrentLocationMarker.position = driverCurretLocation
                
                
                // create driver path
                fetchRoute(from: driverCurretLocation , to: MapViewController.selectedSourceLocation, isPathForUser: false)
                
            }
            
            if(MapViewController.driverPathCoordinatesArray.count == 0){return}
            
            var isOnPath = false
            var index = 0
            
            for i in 0..<MapViewController.driverPathCoordinatesArray.count{
                
                if(MapViewController.driverPathCoordinatesArray[i].latitude == deiverCurrenTrimedLocation.latitude && MapViewController.driverPathCoordinatesArray[i].longitude == deiverCurrenTrimedLocation.longitude ){
                    
                    isOnPath = true
                    index = i
                    break
                }
            }
            
            if(isOnPath){
                print("driver on path")
                
                //Creating new path from the current location to the destination
                
                let newPath = GMSMutablePath()
                
                for i in index..<Int(MapViewController.testPathCoordinatesArray.count){
                    newPath.add(MapViewController.testPathCoordinatesArray[i])
                }
                
                MapViewController.polylineForDriver.map = nil
                MapViewController.polylineForDriver = GMSPolyline(path: newPath)
                MapViewController.polylineForDriver.strokeColor = #colorLiteral(red: 0.9764705882, green: 0.6431372549, blue: 0.1058823529, alpha: 1)//UIColor.red
                MapViewController.polylineForDriver.strokeWidth = 3.0
                MapViewController.polylineForDriver.map = self.mapView
                
                
            }
            else{
                fetchRoute(from: driverCurretLocation , to: MapViewController.selectedSourceLocation, isPathForUser: false)
            }
        }
    }
    
    
    func carAnimactionAToBCordinates(a :CLLocationCoordinate2D , b : CLLocationCoordinate2D ){
        
        MapViewController.driverCurrentLocationMarker.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: a, toCoordinate: b))
        MapViewController.driverCurrentLocationMarker.map = mapView
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(callBackTime)
        MapViewController.driverCurrentLocationMarker.position = b
        CATransaction.commit()
        
        
    }
    
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    
}



extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}


//MARK:- CustomAlertDelegate

extension MapViewController:customeAlertDelegate{
    
    func btnOkClicked() {
        let strbd = staticClass.getStoryboard_Main()
        let vc = strbd?.instantiateViewController(withIdentifier: "DriverRatingVC") as! DriverRatingVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}


//MARK:- WaitAndCancelRide Delegate

extension MapViewController:WaitAndCancelRideVCDelegate{
    
    func cancelRide() {
        self.timeCount = 500
        timer?.invalidate()
        self.isCanceled = true
        self.resetMapViewController()
        //        self.showToast(message: "Ride Cancel Successfully", seconds: 3)
    }
    
}
