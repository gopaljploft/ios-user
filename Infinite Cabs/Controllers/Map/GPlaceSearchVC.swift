//
//  GPlaceSearchVC.swift
//  Infinite Cabs
//
//  Created by Apple on 10/11/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


protocol GooglePlacesDelegate {
    func googlePlaces(mainText:String, secondaryText:String, lat:Double, lng:Double)
}

class GPlaceSearchVC: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var viewSearchContainer: UIView!
    
    
    var addresArray =  [[String]] ()
    var searchedPlaceData = JSON()
    var lastTime = TimeInterval()
    
    var mainText:String = ""
    var secondaryText:String = ""
    
    var delegate:GooglePlacesDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSearchContainer.layer.cornerRadius = 12 
        viewSearchContainer.clipsToBounds = true
        
        txtSearch.delegate = self
        txtSearch.inputAccessoryView = self.methodAddToolbar()
        getSearchPlaceApiData()
    }
    
    
    //MARK:- ACTION
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSearchTapped(_ sender: UIButton) {
        
    }
    
    
    
    
    func getSearchPlaceApiData(){
        
        let differenceTime = Date().timeIntervalSince1970 - lastTime
        
        print(differenceTime)
        
        
        if(differenceTime < 1.0){
            return
        }
        
        let token = ""
        let textForSearch = txtSearch.text!
        
        
        
        if(textForSearch  ==  ""){
            self.searchedPlaceData = JSON()
            self.tbl.reloadData()
            return
        }
        
        
        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(textForSearch)&sensor=\(token.description)&components=country:au&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        
//        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(textForSearch)&sensor=\(token.description)&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        
        let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        
        
        Alamofire.request( url , method: .get , parameters: nil , headers : nil).responseJSON(completionHandler: { (response) in
            
            print(response)
            
            if(response.result.isSuccess){
                
                let jsondata :JSON = JSON(response.data ?? "")
                
                if(jsondata != ""){
                    self.searchedPlaceData =  jsondata["predictions"]
                    self.tbl.reloadData()
                }
            }
        })
    }
    
    
    
    
    //MARK:- Get Lat Long From Address
    
    func getlatLongFromAddress(placeFullNameStr : String){
        
        guard let selectedAddress = placeFullNameStr.addingPercentEncoding(withAllowedCharacters: .symbols) else {
            print("Error. cannot cast name into String")
            return
        }
        
        
        //print(selectedAddress)
        let urlString =  "https://maps.googleapis.com/maps/api/geocode/json?address=\(selectedAddress)&sensor=false&key=AIzaSyAekPLq6U-9oTyxvwSI_DmtcZSsxgoKP9o"
        
        let url = URL(string: urlString)
        
        Alamofire.request(url!, method: .get, headers: nil)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case.success(let value):
                    let json = JSON(value)
                    // print(json)
                    
                    let lat = json["results"][0]["geometry"]["location"]["lat"].doubleValue
                    let lng = json["results"][0]["geometry"]["location"]["lng"].doubleValue
                    
                    self.delegate?.googlePlaces(mainText: self.mainText, secondaryText: self.secondaryText, lat: lat, lng: lng)
                    
                    self.navigationController?.popViewController(animated: true)
                    
                case.failure(let error):
                    print("\(error.localizedDescription)")
                }
                
        }
        
    }
    
    
    
    
    
}

//MARK:- Textfield Delegate
extension GPlaceSearchVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        lastTime = Date().timeIntervalSince1970
        
        
        let seconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.getSearchPlaceApiData()
        }
        
        return true
    }
}


//MARK:- TableView Delegate And DataSource

extension GPlaceSearchVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedPlaceData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GPlaceTVCell") as! GPlaceTVCell
        
        cell.placeNameLabel.text = searchedPlaceData[indexPath.row]["structured_formatting"]["main_text"].stringValue
        cell.placeDetailLabel.text = searchedPlaceData[indexPath.row]["structured_formatting"]["secondary_text"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.mainText = searchedPlaceData[indexPath.row]["structured_formatting"]["main_text"].stringValue
        self.secondaryText = searchedPlaceData[indexPath.row]["structured_formatting"]["secondary_text"].stringValue
        getlatLongFromAddress(placeFullNameStr: searchedPlaceData[indexPath.row]["description"].stringValue)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
