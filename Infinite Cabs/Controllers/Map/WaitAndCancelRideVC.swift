//
//  WaitAndCancelRideVC.swift
//  Infinite Cabs
//
//  Created by Apple on 19/01/21.
//  Copyright © 2021 micro. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON



protocol WaitAndCancelRideVCDelegate {
    func cancelRide()
}

class WaitAndCancelRideVC: UIViewController {

    @IBOutlet weak var viewC: UIView!
    @IBOutlet weak var imgGif: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblMsg: UILabel!
    
    var delegate:WaitAndCancelRideVCDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let imageData = NSData(contentsOf: Bundle.main.url(forResource: "car-middle1", withExtension: "gif")!)
        let animatedImage = UIImage.gif(data: imageData! as Data)//UIImage.gifWithData(imageData!)
        imgGif.image = animatedImage
        
        viewC.layer.cornerRadius = 8
        viewC.clipsToBounds = true
        
        btnCancel.layer.cornerRadius = 4
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = UIColor.lightGray.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissSelf), name: Notification.Name("DismissWaitAndCancelRideVC"), object: nil)
        
     
    }
    
    
    @objc func dismissSelf(){
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        } 
    }
    
    //MARK:- ACTIONS
    
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        cancelRideApi { (success) in
            
            if success{
                
                self.delegate?.cancelRide()
                self.dismissSelf()
            }
            
        }
         
    }
    
    
    
    private func cancelRideApi(completion:@escaping(Bool) -> ()){
          //userCancelRide
//          Hud.shared.showHud()
//        ActivityIndicatorWithLabel.shared.showProgressView(uiView: self.view)
          
          let userID = global.userId
          let dictKeys = [ "user_id" : userID,
                           "driver_id" : MapViewController.AppData["driverId"] ?? "" ,//MapViewController.selectedDriverID,
                          "booking_id" : MapViewController.AppData["bookingId"] ?? "" ,
                        "reasonId" : "0" ,
                        "type" : "user"//,
//                        "reason" : reasonField.text ?? ""
                      ] as [String : Any]


             let url = "\(Constants.AppLinks.API_BASE_URL)\(Constants.ApiUrl.declinedBookings.rawValue)"

             Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in

//                Hud.shared.hideHud()
//                ActivityIndicatorWithLabel.shared.hideProgressView()
                 do {
                     let json: JSON = try JSON(data: response.data!)

                     if (json["statusCode"].stringValue == "200"){

                        MapViewController.userCanCreateNewRide = true
                        
//                      NotificationCenter.default.post(Notification(name: Notification.Name("resetMapViewControllerAllData")))
                        
//                        self.showToast(message: "Ride Cancel Successfully", seconds: 3)
                        
//                        UIApplication.shared.keyWindow?.rootViewController?.showToast(message: "Ride Cancel Successfully", seconds: 3)
                        
//                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.5) {
                            completion(true)
//                        }
                        
                        
                        
//                        self.API_sessionDel{ (success) in
//
//                            if success{
//
//                            }else{
//
//
//                            }
//
//                        }
                        
                        
                     }
                     else{
                         // error
                         self.showAlert(title: "Error", message: json["message"].stringValue)
                        completion(false)
                        
                         }

                 } catch {
                     // show a alert
//                     Hud.shared.hideHud()
                     ActivityIndicatorWithLabel.shared.hideProgressView()
                     self.showAlert(title: "Error", message: "Please check network connection")
                    completion(false)
                 }
             })
          
          
          
      }

    

    
    private func API_sessionDel(completion:@escaping(Bool) -> ()){
           
//           Hud.shared.showHud()
        ActivityIndicatorWithLabel.shared.hideProgressView()
           
           let url = Constants.AppLinks.session_Delete_Url + "twiCall/session_delete.php"//Constants.ApiUrl.sessionDel.rawValue
           let dictKeys = [ "bookingId" : MapViewController.AppData["bookingId"] ?? ""]

           Alamofire.request(url , method: .post, parameters: dictKeys , headers:nil).responseJSON(completionHandler: { (response) in
//               Hud.shared.hideHud()
            ActivityIndicatorWithLabel.shared.hideProgressView()
               do {
                   let json: JSON = try JSON(data: response.data!)

         
                   if (json["statusCode"].stringValue == "200"){
                       
//                    self.cancelRideReasonData =  json["data"]
//                    self.cancelRideReasonTableView.reloadData()
                    completion(true)
                   }
                   else{
                       // error
                       self.showAlert(title: "Error", message: json["message"].stringValue)
                   }

               } catch {
                   // show a alert
//                   Hud.shared.hideHud()
                ActivityIndicatorWithLabel.shared.hideProgressView()
                   self.showAlert(title: "Error", message: "Please check network connection")
               }
           })
       }
    
    
}
