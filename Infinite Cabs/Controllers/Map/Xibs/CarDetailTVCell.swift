//
//  CarDetailTVCell.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit

class CarDetailTVCell: UITableViewCell {

    
    @IBOutlet weak var carImageView: UIImageView!
    
    
    
    @IBOutlet weak var priceLabel: UILabel!
    
    
    @IBOutlet weak var numberOfSeateLabel: UILabel!
    
    @IBOutlet weak var carNameLabel: UILabel!
    
    @IBOutlet weak var viewC: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewC.layer.cornerRadius = 8
        viewC.clipsToBounds = true
//        self.contentView.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
