//
//  DriverPickDetailTVCell.swift
//  Infinite Cabs
//
//  Created by Apple on 03/01/20.
//  Copyright © 2020 micro. All rights reserved.
//

import UIKit
import Cosmos

class DriverPickDetailTVCell: UITableViewCell {

    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var rattingLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var viewCosmos: CosmosView!
    
    @IBOutlet var viewCsmsCon: UIView!
    @IBOutlet var viewCarNumber: UIView!
    @IBOutlet var viewImg: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewCsmsCon.layer.borderColor = UIColor(named: "lightGraySet")?.cgColor//UIColor.lightGray.cgColor
        viewCsmsCon.layer.borderWidth = 1
        
        viewImg.layer.cornerRadius = viewImg.frame.height/2
        viewImg.layer.borderColor = UIColor(named: "lightGraySet")?.cgColor
        viewImg.layer.borderWidth = 1
        
        
        viewCarNumber.layer.borderColor =  UIColor(named: "lightGraySet")?.cgColor
        viewCarNumber.layer.borderWidth = 1
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
